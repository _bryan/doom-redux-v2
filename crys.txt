  Entangling particles might sound like
something you need a lot of expensive
equipment to do. Infact, anybody at home
that has access to basic chemicals and
legally obtainable simple laser pointer.

  When most people think of crystals,
they'll most likely imagine pretty
colored hard rocks with rigid and neat
features. In reality, any solid matter
whos atoms are arranged in a high order
structure with a crystal lattice is
technically a crystal. 
^^^
  The process of growing crystals,
appropriately called crystallization,
is using an intermediate fluid that
contains dissolved materials to make
a crystalline structure.

  Crystal structures can be of use in
more ways than one. For the purposes
of this text, we'll be creating small
crystals we can use to entangle two
photons shot from a laser.
^^^
  Properties of a crystal will always
be variable depending on a range of
conditions, but one thing that is 
generally the same is that particular
chemicals will produce certain colors.
Usually, a sulfate left to sit in water
for about a day will begin to form
small crystals at the bottom of the jar.

  Which color to pick is a matter of
preference and the color of the laser
you have. Shooting green photons at
a green crystal will mostly likely
aborb the light as opposed to letting
it pass through. 
^^^
  The easiest crystals to make are
green and blue made of Ammonium
Sulfate [\cU(NH4)2SO4\cJ] and either Iron 
[\cQFeSO4\cJ] or Copper [\cBCuSO4\cJ] Sulfate
respectively dissolved in water. 
Other choices for example are
Zinc Sulfate [\cUZnSO4\cJ] for a clear white
crystal, or Chrome Alum [\cTKCr(SO4)2\cJ]
for purple. 
^^^
  Be patient as this process takes
mostly time. After some time has
passed a crystal large enough will
eventually form, which will be the
starting point for growing a crystal
that can be used in our experiment.
Make sure the seed crystal is suspended
in the solution and wait for it become
of proper size.

  The next part of this lesson will be
splitting the particles and actually
capturing the entangled pairs, which
is the real hard part.
^^^
  
