vec2 GetTexCoord()
{
	vec2 texCoord = vTexCoord.st;

	const float pi = 3.14159265358979323846;

	float xcoord = sin(pi * 2.0 * (texCoord.y + timer * 0.125)) * 0.1;
	texCoord.x += xcoord;

	texCoord.y += cos(pi * 2.0 * (texCoord.x + timer * 0.125)) * 1;

	return texCoord;
}

vec4 ProcessTexel()
{
	return getTexel(GetTexCoord());
}