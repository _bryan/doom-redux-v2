
class BookRein1Pickup : Actor {
	default {
		radius 6;
		height 15;
		scale 0.5;
	}
	states {
		spawn:
			VBKA A -1;
			Stop;
	}
	override bool used(Actor who) {
		who.GiveInventoryType("BookRein1");
		who.A_StartSound("misc/i_pkup");
		self.destroy();
		return true;
	}
}

class BookRein2Pickup : Actor {
	default {
		radius 6;
		height 15;
		scale 0.5;
	}
	states {
		spawn:
			VBKA B -1;
			Stop;
	}
	override bool used(Actor who) {
		who.GiveInventoryType("BookRein2");
		who.A_StartSound("misc/i_pkup");
		self.destroy();
		return true;
	}
}

class BookRein3Pickup : Actor {
	default {
		radius 6;
		height 15;
		scale 0.5;
	}
	states {
		spawn:
			VBKA C -1;
			Stop;
	}
	override bool used(Actor who) {
		who.GiveInventoryType("BookRein3");
		who.A_StartSound("misc/i_pkup");
		self.destroy();
		return true;
	}
}

class BookRein4Pickup : Actor {
	default {
		radius 6;
		height 15;
		scale 0.5;
	}
	states {
		spawn:
			VBKA D -1;
			Stop;
	}
	override bool used(Actor who) {
		who.GiveInventoryType("BookRein4");
		who.A_StartSound("misc/i_pkup");
		self.destroy();
		return true;
	}
}

class Fable1Pickup : Actor {
	default {
		radius 12;
		height 4;
		scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
	override bool used(Actor who) {
		who.GiveInventoryType("Fable1");
		who.A_StartSound("misc/i_pkup");
		self.destroy();
		return true;
	}
}

class BookRein1 : Inventory {
	default {
		+INVENTORY.INVBAR
		Inventory.MaxAmount 1;
		Inventory.MaxAmount 1;
		Inventory.Icon "VBKAA0";
	}
	override bool use(bool pickup) {
		GenericMenu.SetMenu("Book1Menu");
		return false;
	}
}

class BookRein2 : Inventory {
	default {
		+INVENTORY.INVBAR
		Inventory.MaxAmount 1;
		Inventory.MaxAmount 1;
		Inventory.Icon "VBKAB0";
	}
	override bool use(bool pickup) {
		GenericMenu.SetMenu("Book2Menu");
		return false;
	}
}

class BookRein3 : Inventory {
	default {
		+INVENTORY.INVBAR
		Inventory.MaxAmount 1;
		Inventory.Icon "VBKAC0";
	}
	override bool use(bool pickup) {
		GenericMenu.SetMenu("Book3Menu");
		return false;
	}
}

class Fable1 : Inventory {
	default {
		+INVENTORY.INVBAR
		Inventory.MaxAmount 1;
		Inventory.ICON "SCRLA0";
	}
	override bool use(bool pickup) {
		GenericMenu.SetMenu("Fable1Menu");
		return false;
	}
}

class BookRein4 : Inventory {
	default {
		+INVENTORY.INVBAR
		Inventory.MaxAmount 1;
		Inventory.MaxAmount 1;
		Inventory.Icon "VBKAD0";
	}
	override bool use(bool pickup) {
		GenericMenu.SetMenu("Book4Menu");
		return false;
	}
}


class Book1Menu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("b1");
		BookPage cover = new ("BookPage");
		cover.addEmptyLines(2);
		cover.lines.push("The Story of");
		cover.lines.push("the Fox Reynaerde");
		cover.addEmptyLines(12);
		cover.lines.push("                                           Chapter #1");
		contents.pages.insert(0, cover);
		return contents;
	}
}

class Book2Menu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("b2");
		BookPage cover = new ("BookPage");
		cover.addEmptyLines(2);
		cover.lines.push("The Story of");
		cover.lines.push("the Fox Reynaerde");
		cover.addEmptyLines(12);
		cover.lines.push("                                           Chapter #2");
		contents.pages.insert(0, cover);
		return contents;
	}
}

class Book3Menu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("b3");
		BookPage cover = new ("BookPage");
		cover.addEmptyLines(2);
		cover.lines.push("The Story of");
		cover.lines.push("the Fox Reynaerde");
		cover.addEmptyLines(12);
		cover.lines.push("                                           Chapter #3");
		contents.pages.insert(0, cover);
		return contents;
	}
}

class Book4Menu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("b4");
		BookPage cover = new ("BookPage");
		cover.addEmptyLines(2);
		cover.lines.push("The Story of");
		cover.lines.push("the Fox Reynaerde");
		cover.addEmptyLines(12);
		cover.lines.push("                                           Chapter #4");
		contents.pages.insert(0, cover);
		return contents;
	}
}

class Fable1Menu : BookMenu {
	override BookContents createContents() {
		return createBook("fabel");
	}
}

