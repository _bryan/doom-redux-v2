class BoxBase : Actor {
	default {
		+SHOOTABLE
		+NOBLOOD
		+NOTAUTOAIMED
		scale 3;
	}

	states {
		BoxBroke:
			TNT1 A 0;
			Stop;
	}

	int hasContents;

	override void PostBeginPlay() {
		super.PostBeginPlay();
		double scl = (random(50, 100) / 100.0) * 1.2;
		self.scale = (scl, scl);
		hasContents = random(0, 4);
	}

	override int damagemobj(actor inflictor, actor source, int damage, name mod, int flags, double angle) {
		hasContents = hasContents - 1;
		if (hasContents <= 0) {
			self.Destroy();

		}
			//self.SetStateLabel("BoxBroke");
			//self.A_StartSound("skull/death", CHAN_AUTO);
		return super.damagemobj(inflictor, source, damage, mod, flags, angle);
		
	}

	override bool Used(Actor who) {
		String items[5] = { "HealthBonus", "Clip", "Shell", "Cell", "ArmorBonus" };
		if (hasContents <= 0) {
			self.destroy();
		}
		else {
			int rng = random(0, 4);
			Actor itm = Actor.Spawn(items[rng], self.pos, true);
			itm.vel = (
				random(-3, 3),
				random(-3, 3),
				8
			);
			self.vel = (
				0,
				0,
				2
			);
			hasContents = hasContents - 1;
		}

		return true;
	}
}

class BoxA : BoxBase {
	states {
		spawn:
			BBOX A -1;
			Stop;
	}
}

class BoxB : BoxBase {
	states {
		spawn:
			BBOX B -1;
			Stop;
	}
}

class BoxC : BoxBase {
	states {
		spawn:
			BBOX C -1;
			Stop;
	}
}

class BoxSpawner : RandomSpawner {
	default {
		dropitem "BoxA";
		dropitem "BoxB";
		dropitem "BoxC";
	}
	states {
		spawn:
			BBOX A 0;
			Stop;
	}
}