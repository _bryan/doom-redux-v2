
class DeskFan : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			DFAN A -1;
			Stop;
	}
}

class FireExtingusher : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			FREX A -1;
			Stop;
	}
}

class DeskLamp : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			LAPD A -1;
			Stop;
	}
}

class MopBucket : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			MOPB A -1;
			Stop;
	}
}

class BlueBin : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			OBIN A -1;
			Stop;
	}
}

class GreenBin : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			OBIN B -1;
			Stop;
	}
}

class PhoneBlack : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			PHON A -1;
			Stop;
	}
}

class PhoneBlue : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			PHON B -1;
			Stop;
	}
}


class PhoneRed : Actor {
	default {
		+NOGRAVITY
	}
	states {
		spawn:
			PHON C -1;
			Stop;
	}
}
