
class CoffeeMachine : Actor {
	ChemistryRecipe crafting;
	int currentTime;
	bool busy;

	default {
		//+NOGRAVITY
		Radius 12;
		Height 20;

	}
	states {
		spawn:
			COFF A -1;
			Stop;
		cooking:
			COFF A 35 {
				int nextTic = random(3, 8);
				self.vel += (0, 0, random(1, 3));
				A_SetTics(nextTic);
				A_StartSound("coffee/brew", 0, CHANF_NOSTOP);
			}
			Loop;
	}

	override void Tick() {
		super.Tick();
		if (busy) {
			for (int i = 0; i < random(5, 15); i++) {
				self.A_SpawnParticle(
					crafting.randomColor(), 
					SPF_RELATIVE | SPF_FULLBRIGHT, 
					35, 
					1, 
					0, 
					0, // xoff
					0, // yoff 
					12, // zoff
					random(-1, 1), // xvel
					random(-1, 1), // yvel
					random(-3, 3) // zvel
				);
			}

			currentTime--;
			if (currentTime <= 0) {
				busy = false;
				SetStateLabel("spawn");
				Actor itm = Actor.spawn(crafting.result, self.pos);
				itm.angle = self.angle;
				Vector3 ang = itm.Vec3Angle(8, itm.angle) - itm.pos;
				itm.vel += ang;
			}
		}
	}

	override bool Used(Actor who) {
		ChemistryHandler hndl = ChemistryHandler(EventHandler.Find("ChemistryHandler"));
		hndl.beingUsed[consoleplayer] = self;
		GenericMenu.SetMenu("ChemistryMenu");
		return true;
	}
}