
class ChemistryHandler : EventHandler {

	Array<ChemistryRecipe> unlocked;
	Array<ChemistryRecipe> recipes;
	CoffeeMachine beingUsed[8];

	override void WorldLoaded (WorldEvent e) {

		ChemistryRecipe mohr = new ("ChemistryRecipe");
		mohr.name = "Green crystal";
		mohr.timecost = (35 * 10);
		mohr.result = "BottleA";
		mohr.addRequirement("BottleItemA", 1);
		mohr.addRequirement("BottleItemC", 1);
		mohr.addRequirement("BottleItemB", 3);
		mohr.colors.push("#31ac33");
		mohr.colors.push("#2840f0");
		mohr.colors.push("#979797");
		recipes.push(mohr);
		unlocked.push(mohr);

		ChemistryRecipe deepblue = new ("ChemistryRecipe");
		deepblue.name = "Blue crystal";
		deepblue.timecost = (35 * 5);
		deepblue.result = "BottleA";
		deepblue.addRequirement("BottleItemD", 1);
		deepblue.addRequirement("BottleItemB", 3);
		deepblue.colors.push("#0034c9");
		deepblue.colors.push("#2840f0");
		recipes.push(deepblue);
		unlocked.push(deepblue);

		ChemistryRecipe healthBonus = new ("ChemistryRecipe");
		healthBonus.name = "Health Bonus";
		healthBonus.timecost = 35;
		healthBonus.result = "HealthBonus";
		healthBonus.addRequirement("BottleItemB", 1);
		healthBonus.addRequirement("BottleItemH", 1);
		healthBonus.colors.push("#2840f0");
		healthBonus.colors.push("#c3c3c3");
		recipes.push(healthBonus);
		unlocked.push(healthBonus);

		ChemistryRecipe armorBonus = new ("ChemistryRecipe");
		armorBonus.name = "Armor Bonus";
		armorBonus.timecost = 35;
		armorBonus.result = "ArmorBonus";
		armorBonus.addRequirement("BottleItemB", 1);
		armorBonus.addRequirement("BottleItemG", 1);
		armorBonus.colors.push("#2840f0");
		armorBonus.colors.push("#644a0b");
		recipes.push(armorBonus);
		unlocked.push(armorBonus);

	}

	ChemistryRecipe findRecipe(String n) {
		for(int i = 0; i < unlocked.size(); i++) {
			ChemistryRecipe next = unlocked[i];
			if (next.name == n) {
				return next;
			}
		}
		return NULL;
	}

	override void NetworkProcess (ConsoleEvent e) {
		Array<string> command;
		e.Name.Split (command, ":");
		
		String cmd = command[0];

		if (cmd == "brewitem") {
			String cls = command[1];
			CoffeeMachine m = beingUsed[e.Player];
			m.busy = true;
			m.crafting = findRecipe(cls);
			//console.printf("recipe %s %i", m.crafting.name, m.crafting.timecost);
			m.currentTime = m.crafting.timecost;

			for (int i = 0; i < m.crafting.requires.size(); i++) {
				ChemistryRequirement r = m.crafting.requires[i];
				players[e.Player].mo.TakeInventory(r.classname, r.amount);
			}

			m.SetStateLabel("cooking");
		}

	}

	CoffeeMachine getMachine(int id) const {
		return beingUsed[id];
	}

	bool hasRecipe(string recipe) {
		bool has = false;
		for (int i = 0; i < unlocked.size(); i++) {
			ChemistryRecipe next = unlocked[i];
			if (next.name == recipe) {
				return true;
			}
		}
		return has;
	}

	ui void canCraft(Actor a, Array<ChemistryRecipe> craftable) {
		for(int i = 0; i < unlocked.size(); i++) {
			ChemistryRecipe next = unlocked[i];
			bool can = next.canCraft(a);
			//if (can) {
				craftable.push(next);
			//}
		}
	}

}