
class OrangeKeyPickup : Actor {
	states {
		spawn:
			BKY1 A -1 BRIGHT;
			Stop;
	}

}

class OrangeKeyItem : Inventory {
	default {
		Inventory.MaxAmount 1;
		Inventory.PickupMessage "Picked up back entrance orange key.";
		Inventory.Icon "BKY1A0";
	}
	states {
		spawn:
			BKY1 A -1 BRIGHT;
			Stop;
	}
}

class RedKeyItem : Inventory {
	default {
		Inventory.MaxAmount 1;
		Inventory.PickupMessage "Picked up red employee card.";
		Inventory.Icon "BKY2A0";
	}
	states {
		spawn:
			BKY2 A -1 BRIGHT;
			Stop;
	}
}

class RedKeyPickup : Actor {
	states {
		spawn:
			BKY2 A -1 BRIGHT;
			Stop;
	}
	override bool Used(Actor who) {
		who.GiveInventory("RedKeyItem", 1);
		self.destroy();
		return true;
	}
}

class YellowKeyItem : Inventory {
	default {
		Inventory.MaxAmount 1;
		Inventory.PickupMessage "Picked up yellow employee card.";
		Inventory.Icon "BKY3A0";
	}
	states {
		spawn:
			BKY3 A -1 BRIGHT;
			Stop;
	}
}

class GreenKeyItem : Inventory {
	default {
		Inventory.MaxAmount 1;
		Inventory.PickupMessage "Picked up green employee card.";
		Inventory.Icon "BKY4A0";
	}
	states {
		spawn:
			BKY4 A -1 BRIGHT;
			Stop;
	}
}

class GreenRearKeyItem : Inventory {
	default {
		Inventory.MaxAmount 1;
		Inventory.PickupMessage "Picked up green rear exit key.";
		Inventory.Icon "BKY5A0";
	}
	states {
		spawn:
			BKY5 A -1 Bright;
			Stop;
	}
}