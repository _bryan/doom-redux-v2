
class CanBase : Actor {
	default {
		+SHOOTABLE
		+NOBLOOD
		+PUSHABLE
		Mass 1;
		Radius 3;
		Height 3;
	}
	states {
		CanBroke:
			TNT1 A 0;
			Stop;
	}
	override int damagemobj(actor inflictor, actor source, int damage, name mod, int flags, double angle) {
		self.SetStateLabel("CanBroke");
		self.A_StartSound("sfx/glass/break", CHAN_AUTO);
		return super.damagemobj(inflictor, source, damage, mod, flags, angle);
	}
}

class CanA : CanBase {
	states {
		spawn:
			BCAN A -1;
			Stop;
	}
}
class CanB : CanBase {
	states {
		spawn:
			BCAN B -1;
			Stop;
	}
}
class CanC : CanBase {
	states {
		spawn:
			BCAN C -1;
			Stop;
	}
}
class CanD : CanBase {
	states {
		spawn:
			BCAN D -1;
			Stop;
	}
}
class CanE : CanBase {
	states {
		spawn:
			BCAN E -1;
			Stop;
	}
}

class CanSpawner : RandomSpawner {
	default {
		dropitem "CanA";
		dropitem "CanB";
		dropitem "CanC";
		dropitem "CanD";
		dropitem "CanE";
	}
}