
class BaseComputer : Actor {
	property MonitorImage: MonitorImage;
	property ScreenImage: ScreenImage;
	property HDDImage: HDDImage;
	property PowerImage: PowerImage;

	property ScreenLines: ScreenLines;
	property ScreenChars: ScreenChars;
	
	property MonitorPosX: MonitorPosX;
	property MonitorPosY: MonitorPosY;
	
	property ScreenPosX: ScreenPosX;
	property ScreenPosY: ScreenPosY;

	property ButtonPosX: ButtonPosX;
	property ButtonPosY: ButtonPosY;

	property HDDPosX: HDDPosX;
	property HDDPosY: HDDPosY;

	property PowerPosX: PowerPosX;
	property PowerPosY: PowerPosY;

	String monitorImage;
	String screenImage;
	String hddImage;
	String PowerImage;

	int ScreenLines;
	int ScreenChars;

	int MonitorPosX;
	int MonitorPosY;

	int ButtonPosX;
	int ButtonPosY;

	int HDDPosX;
	int HDDPosY;

	int PowerPosX;
	int PowerPosY;

	


}