

class CoffeeCupHandler : EventHandler {

	override void OnRegister() {

	}

	override void WorldTick() {
		
	}

	bool CoffeeCupUsed (Actor who, ItemCoffeeCupBase cup) {

		return DefaultCupUsed();
	}

	bool CoffeeCupShot (Actor who, CoffeeCupBase cup) {
		return true;
	}

	bool DefaultCupUsed() {
		console.printf("Nothing happened.");
		return false;
	}
}


class CoffeeCupBase : Actor {
		default {
		+SHOOTABLE
		+NOBLOOD
		+PUSHABLE
		+NOTAUTOAIMED
		Mass 1000;
		Pushfactor 0.07;
		Radius 3;
		Height 8;
		Scale 0.45;
	}
	override int damagemobj(actor inflictor, actor source, int damage, name mod, int flags, double angle) {
		self.onShot(inflictor);
		return super.damagemobj(inflictor, source, damage, mod, flags, angle);
	}
	override bool used(Actor who) {
		who.GiveInventoryType(getItemClass());
		who.A_StartSound("misc/i_pkup");
		self.destroy();
		return true;
	}
	virtual Class<BrokenCoffeeCupBase> getCupClass() {
		return "BrokenCoffeeCupBase";
	}
	virtual Class<ItemCoffeeCupBase> getItemClass() {
		return "ItemCoffeeCupBase";
	}
	void onShot(actor who) {
		let h = CoffeeCupHandler(EventHandler.Find("CoffeeCupHandler"));
		if (h.CoffeeCupShot(who, self)) {
			let a = Actor.spawn(self.getCupClass());
			a.A_StartSound("sfx/mug/break", CHAN_AUTO);
			a.SetOrigin(self.pos, false);
			self.destroy();
		}
	}
}

class BrokenCoffeeCupBase : Actor {
	default {
		scale 0.45;
	}
}

class ItemCoffeeCupBase : Inventory {
	default {
		+INVENTORY.INVBAR
		Inventory.Amount 1;
		Inventory.MaxAmount 99;
		scale 0.45;	
	}
	virtual Class<CoffeeCupBase> getPickupClass() {
		return "CoffeeCupBase";
	}
	override bool use(bool pickup) {
		let h = CoffeeCupHandler(EventHandler.Find("CoffeeCupHandler"));
		return h.CoffeeCupUsed(owner, self);
	}
	override void OnDrop(Actor who) {
		let a = Actor.spawn(self.getPickupClass(), who.pos, true);
		self.destroy();
	}
}

class RandomCoffeeCup : RandomSpawner {
	default {
		dropitem "BlackCoffeeCup";
		dropitem "BlueCoffeeCup";
		dropitem "WhiteCoffeeCup";
		dropitem "RedCoffeeCup";
		dropitem "CyanCoffeeCup";
		dropitem "GreenCoffeeCup";
		dropitem "BrownCoffeeCup";
		dropitem "CrimsonCoffeeCup";
	}
	states {
		spawn:
			CCBK A -1;
			Stop;
	}
}

class BlackCoffeeCup : CoffeeCupBase {
	states { spawn: CCBK A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenBlackCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "BlackCoffeeCupItem"; }
}

class BrokenBlackCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBBK A -1; Stop; }
}

class BlackCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCBKA0"; }
	states { spawn:	CCBK A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "BlackCoffeeCup"; }
}




class BlueCoffeeCup : CoffeeCupBase {
	states { spawn: CCBL A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenBlueCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "BlueCoffeeCupItem"; }
}

class BrokenBlueCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBBL A -1; Stop; }
}

class BlueCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCBLA0"; }
	states { spawn:	CCBL A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "BlueCoffeeCup"; }
}






class WhiteCoffeeCup : CoffeeCupBase {
	states { spawn: CCWT A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenWhiteCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "WhiteCoffeeCupItem"; }
}

class BrokenWhiteCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBWT A -1; Stop; }
}

class WhiteCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCWTA0"; }
	states { spawn:	CCWT A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "WhiteCoffeeCup"; }
}



class RedCoffeeCup : CoffeeCupBase {
	states { spawn: CCRD A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenRedCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "RedCoffeeCupItem"; }
}

class BrokenRedCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBRD A -1; Stop; }
}

class RedCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCRDA0"; }
	states { spawn:	CCRD A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "RedCoffeeCup"; }
}



class CyanCoffeeCup : CoffeeCupBase {
	states { spawn: CCCY A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenCyanCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "CyanCoffeeCupItem"; }
}

class BrokenCyanCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBCY A -1; Stop; }
}

class CyanCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCCYA0"; }
	states { spawn:	CCCY A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "CyanCoffeeCup"; }
}


class GreenCoffeeCup : CoffeeCupBase {
	states { spawn: CCGR A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenGreenCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "GreenCoffeeCupItem"; }
}

class BrokenGreenCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBGR A -1; Stop; }
}

class GreenCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCGRA0"; }
	states { spawn:	CCGR A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "GreenCoffeeCup"; }
}


class BrownCoffeeCup : CoffeeCupBase {
	states { spawn: CCBW A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenBrownCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "BrownCoffeeCupItem"; }
}

class BrokenBrownCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBBW A -1; Stop; }
}

class BrownCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCBWA0"; }
	states { spawn:	CCBW A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "BrownCoffeeCup"; }
}



class CrimsonCoffeeCup : CoffeeCupBase {
	states { spawn: CCCR A -1; Stop; }
	override Class<BrokenCoffeeCupBase> getCupClass() { return "BrokenCrimsonCoffeeCup"; }
	override Class<ItemCoffeeCupBase> getItemClass() { return "CrimsonCoffeeCupItem"; }
}

class BrokenCrimsonCoffeeCup : BrokenCoffeeCupBase {
	states { spawn: CBCR A -1; Stop; }
}

class CrimsonCoffeeCupItem : ItemCoffeeCupBase {
	default { Inventory.Icon "CCCRA0"; }
	states { spawn:	CCCR A -1; }
	override Class<CoffeeCupBase> getPickupClass() { return "CrimsonCoffeeCup"; }
}
