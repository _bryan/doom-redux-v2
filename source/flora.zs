class CypressTreeBase : Actor {
	default {
		radius 1;
		height 150;
	}
	override void Tick() {}
}

class CypressA : CypressTreeBase {
	states {
		spawn:
			BTRE A -1;
			Stop;
	}
}

class CypressB : CypressTreeBase {
	states {
		spawn:
			BTRE B -1;
			Stop;
	}
}

class CypressC : CypressTreeBase {
	states {
		spawn:
			BTRE C -1;
			Stop;
	}
}

class CypressD : CypressTreeBase {
	states {
		spawn:
			BTRE D -1;
			Stop;
	}
}

class CypressE : CypressTreeBase {
	states {
		spawn:
			BTRE E -1;
			Stop;
	}
}

class CypressF : CypressTreeBase {
	states {
		spawn:
			BTRE F -1;
			Stop;
	}
}

class CypressG : CypressTreeBase {
	states {
		spawn:
			BTRE G -1;
			Stop;
	}
}

class CypressTreeSpawner : RandomSpawner {
	default {
		dropitem "CypressA";
		dropitem "CypressB";
		dropitem "CypressC";
		dropitem "CypressD";
		dropitem "CypressE";
		dropitem "CypressF";
		dropitem "CypressG";
	}
	states {
		spawn:
			BTRE A -1;
			Stop;
	}
}

class ZelkovaTreeBase : Actor {
	default {
		radius 128;
		height 208;
		scale 2;
	}
	override void Tick() {}
}

class ZelkovaA : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ A -1;
			Stop;
	}
}

class ZelkovaB : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ B -1;
			Stop;
	}
}

class ZelkovaC : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ C -1;
			Stop;
	}
}

class ZelkovaD : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ D -1;
			Stop;
	}
}

class ZelkovaE : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ E -1;
			Stop;
	}
}

class ZelkovaF : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ F -1;
			Stop;
	}
}

class ZelkovaG : ZelkovaTreeBase {
	states {
		spawn:
			BTRZ G -1;
			Stop;
	}
}

class ZelkovaTreeSpawner : RandomSpawner {
	default {
		dropitem "ZelkovaA";
		dropitem "ZelkovaB";
		dropitem "ZelkovaC";
		dropitem "ZelkovaD";
		dropitem "ZelkovaE";
		dropitem "ZelkovaF";
		dropitem "ZelkovaG";
	}
	states {
		spawn:
			BTRZ A -1;
			Stop;
	}
}


class OakTreeBase : Actor {
	default {
		radius 1;
		height 512;
	}
	override void PostBeginPlay() {
		scale.y = 1 + frandom(-0.2, 0.2);
		//scale.x = 1 + frandom(-0.5, 0.5);
	}
	override void Tick() {}
}

class OakA : OakTreeBase {
	states {
		spawn:
			BTOA A -1;
			stop;
	}
}

class OakB : OakTreeBase {
	states {
		spawn:
			BTOA B -1;
			stop;
	}
}

class OakC : OakTreeBase {
	states {
		spawn:
			BTOA C -1;
			stop;
	}
}

class OakD : OakTreeBase {
	states {
		spawn:
			BTOA D -1;
			stop;
	}
}

class OakE : OakTreeBase {
	states {
		spawn:
			BTOA E -1;
			stop;
	}
}

class OakF : OakTreeBase {
	states {
		spawn:
			BTOA F -1;
			stop;
	}
}

class OakG : OakTreeBase {
	states {
		spawn:
			BTOA G -1;
			stop;
	}
}

class OakTreeSpawner : RandomSpawner {
	default {
		dropitem "OakA";
		dropitem "OakB";
		dropitem "OakC";
		dropitem "OakD";
		dropitem "OakE";
		dropitem "OakF";
		dropitem "OakG";
	}
	states {
		spawn:
			BTOA A -1;
			Stop;
	}
}

class WillowBase : Actor {
	default {
		radius 1;
		height 110;
	}
	override void Tick() {}
}

class WillowA : WillowBase {
	states {
		spawn:
			BTT2 A -1;
			Stop;
	}
}

class WillowB : WillowBase {
	states {
		spawn:
			BTT2 B -1;
			Stop;
	}
}

class WillowC : WillowBase {
	states {
		spawn:
			BTT2 C -1;
			Stop;
	}
}

class WillowD : WillowBase {
	states {
		spawn:
			BTT2 D -1;
			Stop;
	}
}

class WillowE : WillowBase {
	states {
		spawn:
			BTT2 E -1;
			Stop;
	}
}

class WillowF : WillowBase {
	states {
		spawn:
			BTT2 F -1;
			Stop;
	}
}

class WillowG : WillowBase {
	states {
		spawn:
			BTT2 G -1;
			Stop;
	}
}

class WillowTreeSpawner : RandomSpawner {
	default {
		dropitem "WillowA";
		dropitem "WillowB";
		dropitem "WillowC";
		dropitem "WillowD";
		dropitem "WillowE";
		dropitem "WillowF";
		dropitem "WillowG";
	}
	states {
		spawn:
			BTT2 A -1;
			Stop;
	}
}

class LittleGrassBase : Actor {
	default {
		radius 10;
		height 13;
	}
	override void Tick() {}
}

class LittleGrass1 : LittleGrassBase {
	states {
		spawn:
			GRAS A -1;
			Stop;
	}
}

class LittleGrass2 : LittleGrassBase {
	states {
		spawn:
			GRAS B -1;
			Stop;
	}
}

class LittleGrass3 : LittleGrassBase {
	states {
		spawn:
			GRAS C -1;
			Stop;
	}
}

class LittleGrass4 : LittleGrassBase {
	states {
		spawn:
			PLNT A -1;
			Stop;
	}
}

class LittleGrass5 : LittleGrassBase {
	states {
		spawn:
			PLNT B -1;
			Stop;
	}
}

class LittleGrass6 : LittleGrassBase {
	states {
		spawn:
			PLNT C -1;
			Stop;
	}
}



class LittleGrass7 : LittleGrassBase {
	states {
		spawn:
			PLNT D -1;
			Stop;
	}
}

class LittleGrass8 : LittleGrassBase {
	states {
		spawn:
			PLNT E -1;
			Stop;
	}
}

class LittleGrass9 : LittleGrassBase {
	states {
		spawn:
			PLNT F -1;
			Stop;
	}
}



class LittleGrassSpawner : RandomSpawner {
	default {
		dropitem "LittleGrass1";
		dropItem "LittleGrass2";
		dropItem "LittleGrass3";
		dropitem "LittleGrass4";
		dropItem "LittleGrass5";
		dropItem "LittleGrass6";
		dropitem "LittleGrass7";
		dropItem "LittleGrass8";
		dropItem "LittleGrass9";
	}
	states {
		spawn:
			GRAS A -1;
			Stop;
	}
}

class RockBase : Actor {
	default {
		radius 10;
		height 13;
	}
}

class RockA : RockBase { states { spawn: BRCK A -1; stop; } }
class RockB : RockBase { states { spawn: BRCK B -1; stop; } }
class RockC : RockBase { states { spawn: BRCK C -1; stop; } }
class RockD : RockBase { states { spawn: BRCK D -1; stop; } }

class RockSpawner : RandomSpawner {
	default {
		dropitem "RockA";
		dropitem "RockB";
		dropitem "RockC";
		dropitem "RockD";
	}
}

class OutdoorPotBase : Actor {
	default {
		radius 12;
		height 24;
	}
	override void Tick() {}
}

class OutdoorPotA : OutdoorPotBase {
	states {
		spawn:
			OUPT A -1;
			Stop;
	}
}

class OutdoorPotB : OutdoorPotBase {
	states {
		spawn:
			OUPT B -1;
			Stop;
	}
}

class OutdoorPotC : OutdoorPotBase {
	states {
		spawn:
			OUPT C -1;
			Stop;
	}
}

class OutdoorPotD : OutdoorPotBase {
	states {
		spawn:
			OUPT D -1;
			Stop;
	}
}

class OutdoorPotSpawner : RandomSpawner {
	default {
		dropitem "OutdoorPotA";
		dropitem "OutdoorPotB";
		dropitem "OutdoorPotC";
		dropitem "OutdoorPotD";
	}
}


class GlowingTree1 : Actor {
	default {
		radius 1;
		height 100;
		+NOGRAVITY
	}
	states {
		spawn:
			BTOB ABCDEFEDCB 2;
			BTOB A 0 a_randomleaf();
			Loop;
	}
	action void a_randomleaf() {
		string classname = string.format("GlowingLeaf%d", random(1, 5));
		GlowingLeaf l = GlowingLeaf(Actor.Spawn(classname));	
		l.SetOrigin(self.pos + (0, 0, random(32, -32)), false);
		l.vel = (random(-1, 1), random(-1, 1), random(0, 2));
	}

}

class GlowingLeaf : Actor {
	default {
		radius 1;
		height 1;
		gravity 0.05;
		friction 0.1;
	}

	states {
		goodbye:
			---- # 175 Bright;
			Stop;
	}
}

class GlowingLeaf1 : GlowingLeaf {
	states {
		spawn:
			OBLF A 0;
			Goto goodbye;
	}
}

class GlowingLeaf2 : GlowingLeaf {
	states {
		spawn:
			OBLF B 0;
			Goto goodbye;
	}
}

class GlowingLeaf3 : GlowingLeaf {
	states {
		spawn:
			OBLF C 0;
			Goto goodbye;
	}
}

class GlowingLeaf4 : GlowingLeaf {
	states {
		spawn:
			OBLF D 0;
			Goto goodbye;
	}
}

class GlowingLeaf5 : GlowingLeaf {
	states {
		spawn:
			OBLF E 0;
			Goto goodbye;
	}
}