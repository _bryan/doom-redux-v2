


class TAction {
	virtual ui bool run(TerminalMenu menu) {
		return true;
	}
}

class Println : TAction {
	string line;
	override bool run(TerminalMenu menu) {
		menu.inputBuffer.push(line);
		return true;
	}
}

class Pauser : TAction {
	int ticks;
	int wait;
	override bool run(TerminalMenu menu) {
		ticks++;
		if (ticks % 3) {
			menu.thinking = !menu.thinking;
		}
		if (ticks > wait) {
			menu.thinking = false;
			return true;
		}
		return false;
	}
}

class Noise : TAction {
	string snd;
	override bool run(TerminalMenu menu) {
		menu.MenuSound(snd);
		return true;
	}
}

class ClearBuffer : TAction {
	override bool run(TerminalMenu menu) {
		menu.buffer.clear();
		return true;
	}
}

class Actions {
	static Println println(String input) {
		let act = new ("Println");
		act.line = input;
		return act;
	}

	static Pauser pause(int ticks) {
		let act = new ("Pauser");
		act.wait = ticks;
		return act;
	}

	static Noise noise(String sound) {
		let act = new ("Noise");
		act.snd = sound;
		return act;
	}

	static ClearBuffer clear() {
		let act = new ("ClearBuffer");
		return act;
	}
}

class TerminalTrigger : Inventory {
	default {
		Inventory.MaxAmount 1;
	}
	override void DoEffect() {
		GenericMenu.SetMenu("TerminalMenu");
		self.destroy();
	}
}

class TerminalMenu : GenericMenu {

	TextureID textureScreen;
	TextureID textureButton;
	TextureID textureHDD;
	TextureID texturePower;

	TextureID mon0;
	TextureID mon1;
	TextureID mon2;
	TextureId textureMonitorDraw;

	Array<String> buffer;
	Array<String> inputBuffer;
	Array<TAction> actionQueue;
	Array<String> commands;

	Vector2 startPosition;
	Vector2 cursorPosition;
	Vector2 monitorPosition;
	Vector2 buttonPosition;

	Vector2 btl;
	Vector2 bbr;

	int charWidth;
	int heightLines;
	int lineOffset;

	bool powerOn;
	bool booting;
	bool thinking;

	bool awaitInput;
	int inputx, inputy;

	Font fnt;

	void screenNoise() {
		int index = random(0, 3);
		TextureId randomTex;
		if (index == 0) {
			randomTex = mon0;
		}
		else if (index == 1) {
			randomTex = mon1;
		}
		else {
			randomTex = mon2;
		}

		if (randomTex == textureMonitorDraw) {
			screenNoise();
		}
		else {
			textureMonitorDraw = randomTex;
		}
	}

	void println(string line) {
		screen.drawText(fnt, FONT.CR_WHITE, cursorPosition.x, cursorPosition.y, line, DTA_320x200, 1);
		cursorPosition.x = startPosition.x;
		cursorPosition.y += lineOffset;
	}

	void printBuffer() {
		cursorPosition = startPosition;
		int bSize = clamp(buffer.size(), buffer.size(), heightLines);
		for (int l = 0; l < bSize; l++) {
			string ln = buffer[l];
			self.println(ln);
		}
	}

	void shiftInput() {
		int waiting = inputBuffer.size();
		if (waiting > 0) {
			buffer.push(inputBuffer[0]);
			if (buffer.size() >= heightLines) {
				buffer.delete(0);
			}
			inputBuffer.delete(0);
		}
	}

	void runAction() {
		if (actionQueue.size() > 0) {
			TAction top = actionQueue[0];
			bool done = top.run(self);
			if (done) {
				actionQueue.delete(0);
			}
		}

		if (actionQueue.size() == 0) {
			awaitInput = true;
			inputx = startPosition.x + 8;
			inputy = startPosition.y + (3 * lineOffset);
		}
		else {
			awaitInput = false;
		}
	}

	void drawCommandText() {

	}

	void startShutdown() {
		buffer.clear();
		inputBuffer.clear();
		actionQueue.clear();
		thinking = false;
	}

	void startStartup() {
		actionQueue.push(Actions.noise("comp/click"));
		actionQueue.push(Actions.pause(35));
		//string.format("\cd[\cuRF\cd]\cj \cf%.2f\cj metre%s",b,b==1?"":"s")
		actionQueue.push(Actions.println("\cdUnion Aerospace Company, LLC"));
		actionQueue.push(Actions.println("Copyright (C) \cf1916-1972"));
		actionQueue.push(Actions.pause(35));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.println("Using username \"wsuser\"."));
		actionQueue.push(Actions.println("Authenticating with public key"));
		actionQueue.push(Actions.println(" > \"wsuser-local-key-19710601\"."));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.println("Welcome to UOS 2.12.3"));
		actionQueue.push(Actions.println("(UNIX Time-Sharing System v2)"));
		actionQueue.push(Actions.println(" * Resources"));
		actionQueue.push(Actions.println("  + uasr://documentation"));
		actionQueue.push(Actions.println("  + uasr://management"));
		actionQueue.push(Actions.println("  + uasr://support"));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.println("System load: 0.45"));
		actionQueue.push(Actions.println("Usage of /: 85\% of 50MB"));
		actionQueue.push(Actions.println("Memory usage: 70\%"));
		actionQueue.push(Actions.println("Processes: 5"));
		actionQueue.push(Actions.println("Users logged in: 2"));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.println("20 updates can be installed."));
		actionQueue.push(Actions.println("3 of these are security related."));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.pause(75));
		actionQueue.push(Actions.println("Failed to connect to"));
		actionQueue.push(Actions.println(" > uasr://registry/pkg"));
		actionQueue.push(Actions.pause(35));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.pause(35));
		actionQueue.push(Actions.println("\caUncaug8ht ReferenceError."));
		actionQueue.push(Actions.println(" \cacheckCredentials is not defined."));
		actionQueue.push(Actions.println(" \ca<anonymous> debugger eval code:1"));
		actionQueue.push(Actions.pause(35));
		actionQueue.push(Actions.println(""));
		actionQueue.push(Actions.println("\cfFall back basic auth."));
		actionQueue.push(Actions.pause(75));
		actionQueue.push(Actions.clear());
		actionQueue.push(Actions.println("\cdUP/DOWN Arrow: Command"));
		actionQueue.push(Actions.println("\cdReturn: Execute"));
		actionQueue.push(Actions.println("\c $ "));

	}

	override void Init (Menu parent) {
		super.Init(parent);
		textureScreen = TexMan.checkForTexture("tscr1", TexMan.Type_Any);
		textureButton = TexMan.checkForTexture("tsbt", TexMan.Type_Any);
		textureHDD    = TexMan.checkForTexture("tshd", TexMan.Type_Any);
		texturePower  = TexMan.checkForTexture("tspl", TexMan.Type_Any);

		mon0 = TexMan.checkForTexture("tscr2", TexMan.Type_Any);
		mon1 = TexMan.checkForTexture("tscr3", TexMan.Type_Any);
		mon2 = TexMan.checkForTexture("tscr4", TexMan.Type_Any);

		startPosition = (6, 6);
		monitorPosition = (5, 5);
		buttonPosition = (17, 185);

		btl = (0.163542, 0.925926);
		bbr = (0.186458, 0.973148);

		charWidth = 34;
		heightLines = 22;
		lineOffset = 8;
		fnt = Font.GetFont("smallfont");
		awaitInput = false;
	}

	override bool MenuEvent (int mkey, bool fromController) {
		if (awaitInput) {
			console.printf("%i", mkey);
			if (mkey == 0) {
				actionQueue.push(Actions.pause(75));
				actionQueue.push(Actions.clear());
				actionQueue.push(Actions.println("\cdUP/DOWN Arrow: Command"));
				actionQueue.push(Actions.println("\cdReturn: Execute"));
				actionQueue.push(Actions.println("\c $ "));
			}
		}
		return super.MenuEvent(mkey, fromController);
	}

	override bool MouseEvent (int type, int x, int y) {
		int scw = CVar.FindCVar("vid_scale_customwidth").GetInt();
		int sch = CVar.FindCVar("vid_scale_customheight").GetInt();
		double xn = x / (scw * 1.0);
		double yn = y / (sch * 1.0);
		bool tl = (xn >= btl.x) && (yn >= btl.y);
		bool br = (xn <= bbr.x) && (yn <= bbr.y);
		if (tl && br) {
			powerOn = !powerOn;
			if (powerOn) {
				startStartup();
			}
			else {
				startShutdown();
			}
		}
		return false;
	}

	override void Drawer() {
		PlayerInfo info = players[consoleplayer];

		screen.drawTexture(textureScreen, false, 0, 0, DTA_320x200, 1, DTA_CenterOffset);
		screen.drawTexture(textureButton, false, buttonPosition.x, buttonPosition.y, DTA_320x200, 1, DTA_CenterOffset);
		if (powerOn) {
			screenNoise();
			screen.drawTexture(texturePower, false, 10, 184, DTA_320x200, 1, DTA_CenterOffset);
			screen.drawTexture(textureMonitorDraw, false, monitorPosition.x, monitorPosition.y, DTA_320x200, 1, DTA_CenterOffset);
			printBuffer();
			runAction();

			if (thinking) {
				screen.drawTexture(textureHDD, false, 4, 184, DTA_320x200, 1, DTA_CenterOffset);
			}

			shiftInput();
		}

		super.Drawer();
	}

}