
class HydrogenPickup : RPickup {
	default {
		RPickup.Classname "HydrogenItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cJHydrogen";
	}
	states {
		spawn:
			HYDR A -1;
			Stop;
	}
}

class HydrogenItem : RChemical {
	default {
		RItem.Classname "HydrogenPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "HYDRA0";
		Tag "\cJHydrogen [H 1]";
	}
}