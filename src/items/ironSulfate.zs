class IronSulfatePickup : RPickup {
	default {
		RPickup.Classname "IronSulfateItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cQIron Sulfate";
	}
	states {
		spawn:
			IRSU A -1;
			Stop;
	}
}

class IronSulfateItem : RChemical {
	default {
		RItem.Classname "IronSulfatePickup";
		Inventory.Icon "IRSUA0";
		tag "\cQFeSO4";
	}
}