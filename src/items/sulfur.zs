
class SulfurPickup : RPickup {
	default {
		RPickup.Classname "SulfurItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cKSulfur";
	}
	states {
		spawn:
			SULF A -1;
			Stop;
	}
}

class SulfurItem : RChemical {
	default {
		RItem.Classname "SulfurPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "SULFA0";
		Tag "\cKSulfur [S 16]";
	}
}