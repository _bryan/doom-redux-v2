class ZincSheetPickup : RPickup {
	default {
		RPickup.Classname "ZincSheetItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cWZinc [Zn 30]";
	}
	states {
		spawn:
			ZNSH A -1;
			Stop;
	}
}

class ZincSheetItem : RChemical {
	default {
		RItem.Classname "ZincSheetPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "ZNSHA0";
		Tag "\cWZinc [Zn 30]";
	}
}