
class NoteToJobPickup : RPickup {
	default {
		RPickup.Classname "NoteToJobItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "Note to Job";
		Scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
}

class NoteToJobItem : Readable {
	default {
		RItem.Classname "NoteToJobPickup";
		Readable.MenuName "NoteToJobMenu";
		Inventory.Icon "SCRLA0";
		tag "Note to Job";
	}
}

class NoteToJobMenu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("jobletter");
		return contents;
	}
}