class CopperSulfatePickup : RPickup {
	default {
		RPickup.Classname "CopperSulfateItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cBCopper Sulfate";
	}
	states {
		spawn:
			CPSU A -1;
			Stop;
	}
}

class CopperSulfateItem : RChemical {
	default {
		RItem.Classname "CopperSulfatePickup";
		Inventory.Icon "CPSUA0";
		tag "\cBCuSO4";
	}
}