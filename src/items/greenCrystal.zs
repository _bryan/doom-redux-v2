
class GreenCrystalPickup : RPickup {
	default {
		//Inventory.Icon "BCMAA0";
		//tag "\cHH2O";
		RPickup.Classname "GreenCrystalItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cDGreen crystal";
		Scale 0.25;
	}
	states {
		spawn:
			GRCR A -1;
			Stop;
	}
}

class GreenCrystalItem : CrystalItem {
	default {
		RItem.Classname "GreenCrystalPickup";
		Inventory.Icon "GRCRA0";
		tag "\cDGreen crystal";
	}
	override bool Use(bool pickup) {
		Teleporter found = findNearest();
		if (found) {
			if (hndl.gA != NULL && hndl.gB != NULL) {
				return false;
				console.printf("Green already synchronized.");
			}
			bool placed = found.setCrystal(self);
			if (placed) {
				if (hndl.gA == NULL) {
					console.printf("Teleport node set to green, awaiting synchronization.");
					hndl.gA = found;
				}
				else {
					console.printf("Teleport nodes synchronized with green.");
					hndl.gB = found;
				}
			}
			else {
				if (found.color == "green") {
					console.printf("Node is already green.");
				}
				else {
					console.printf("Couldn't set node to green.");
				}
			}
			return placed;
		}
		return false;
	}
}