class AmmoniumSulfatePickup : RPickup {
	default {
		RPickup.Classname "AmmoniumSulfateItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cCAmmonium Sulfate";
	}
	states {
		spawn:
			ALSU A -1;
			Stop;
	}
}

class AmmoniumSulfateItem : RChemical {
	default {
		RItem.Classname "AmmoniumSulfatePickup";
		Inventory.Icon "ALSUA0";
		tag "\cC(NH4)2SO4";
	}
}