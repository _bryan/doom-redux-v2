
class BatteryPickup : RPickup {
	default {
		RPickup.Classname "BatteryItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cIAA Battery";
		Scale 0.5;
	}
	states {
		spawn:
			BBAT A -1;
			Stop;
	}
}

class BatteryItem : RItem {
	default {
		RItem.Classname "BatteryPickup";
		Inventory.Icon "BBATA0";
		tag "\cIAA Battery";
	}
}