
class CharcoalPickup : RPickup {
	default {
		RPickup.Classname "CharcoalItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cUCharcoal";
	}
	states {
		spawn:
			CHRC A -1;
			Stop;
	}
}

class CharcoalItem : RChemical {
	default {
		RItem.Classname "CharcoalPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "CHRCA0";
		Tag "\cUCharcoal";
	}
}