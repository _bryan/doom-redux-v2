
class Crystals101Pickup : RPickup {
	default {
		RPickup.Classname "Crystals101Item";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "Crystals 101: \cNBasic Crystallisation";
		Scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
}

class Crystals101Item : Readable {
	default {
		RItem.Classname "Crystals101Pickup";
		Readable.MenuName "Crystals101Menu";
		Inventory.Icon "SCRLA0";
		tag "Note to Job";
	}
}

class Crystals101Menu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("crys");
		BookPage cover = new ("BookPage");
		cover.addEmptyLines(2);
		cover.lines.push("Crystals 101");
		cover.lines.push("\cNBasic Crystallisation");
		cover.addEmptyLines(12);
		contents.pages.insert(0, cover);
		return contents;
	}
}