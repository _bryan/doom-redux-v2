class PurpleCrystalPickup : RPickup {
	default {
		RPickup.Classname "PurpleCrystalItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cTPurple crystal";
		Scale 0.25;
	}
	states {
		spawn:
			GRCR D -1;
			Stop;
	}
}

class PurpleCrystalItem : CrystalItem {
	default {
		RItem.Classname "PurpleCrystalPickup";
		Inventory.Icon "GRCRD0";
		tag "\cTPurple crystal";
	}
	override bool Use(bool pickup) {
		Teleporter found = findNearest();
		if (found) {
			if (hndl.pB != NULL) {
				return false;
				console.printf("Purple already synchronized.");
			}

			bool placed = found.setCrystal(self);
			if (placed) {
				hndl.pB = found;
				console.printf("Teleport nodes synchronized with purple.");
			}
			else {
				if (found.color == "purple") {
					console.printf("Node is already purple.");
				}
				else {
					console.printf("Couldn't set node to purple.");
				}
			}
			return placed;
			
		}
		return false;
	}
}