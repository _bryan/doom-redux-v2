
class CodeNotePickup : RPickup {
	default {
		RPickup.Classname "CodeNoteItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "Encoding employee cards";
		Scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
}

class CodeNoteItem : Readable {
	default {
		RItem.Classname "CodeNotePickup";
		Readable.MenuName "CodeNoteMenu";
		Inventory.Icon "SCRLA0";
		tag "Encoding employee cards";
	}
}

class CodeNoteMenu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("inscode");
		return contents;
	}
}