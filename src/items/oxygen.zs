class OxygenPickup : RPickup {
	default {
		RPickup.Classname "OxygenItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cJOxygen";
	}
	states {
		spawn:
			OXYG A -1;
			Stop;
	}
}

class OxygenItem : RChemical {
	default {
		RItem.Classname "OxygenPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "OXYGA0";
		Tag "\cJOxygen [O 10]";
	}
}