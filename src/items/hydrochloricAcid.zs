class HydrochloricAcidPickup : RPickup {
	default {
		RPickup.Classname "HydrochloricAcidItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cJHydrochloric acid";
	}
	states {
		spawn:
			HDYC A -1;
			Stop;
	}
}

class HydrochloricAcidItem : RChemical {
	default {
		RItem.Classname "HydrochloricAcidPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "HDYCA0";
		Tag "\cJHCl";
	}
}