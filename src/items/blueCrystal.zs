
class BlueCrystalPickup : RPickup {
	default {
		RPickup.Classname "BlueCrystalItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cYBlue crystal";
		Scale 0.25;
	}
	states {
		spawn:
			GRCR B -1;
			Stop;
	}
}

class BlueCrystalItem : CrystalItem {
	default {
		RItem.Classname "BlueCrystalPickup";
		Inventory.Icon "GRCRB0";
		tag "\cYBlue crystal";
	}
	override bool Use(bool pickup) {
		Teleporter found = findNearest();
		if (found) {
			if (hndl.bA != NULL && hndl.bB != NULL) {
				return false;
				console.printf("Blue already synchronized.");
			}
			bool placed = found.setCrystal(self);
			if (placed) {
				if (hndl.bA == NULL) {
					console.printf("Teleport node set to blue, awaiting synchronization.");
					hndl.bA = found;
				}
				else {
					console.printf("Teleport nodes synchronized with blue.");
					hndl.bB = found;
				}
			}
			else {
				if (found.color == "blue") {
					console.printf("Node is already blue.");
				}
				else {
					console.printf("Couldn't set node to blue.");
				}
			}
			return placed;
		}
		return false;
	}
}