
class CrystalReportPickup : RPickup {
	default {
		RPickup.Classname "CrystalReportItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "Report R-075";
		Scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
}

class CrystalReportItem : Readable {
	default {
		RItem.Classname "CrystalReportPickup";
		Readable.MenuName "CrystalReportMenu";
		Inventory.Icon "SCRLA0";
		tag "Report R-075";
	}
}

class CrystalReportMenu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("cryt");
		return contents;
	}
}