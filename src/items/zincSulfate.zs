class ZincSulfatePickup : RPickup {
	default {
		RPickup.Classname "ZincSulfateItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cCZinc Sulfate";
	}
	states {
		spawn:
			ZNSU A -1;
			Stop;
	}
}

class ZincSulfateItem : RChemical {
	default {
		RItem.Classname "ZincSulfatePickup";
		Inventory.Icon "ZNSUA0";
		tag "\cCZnSO4";
	}
}