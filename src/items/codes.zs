class CodesPickup : RPickup {
	default {
		RPickup.Classname "CodesItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "Code rotation";
		Scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
}

class CodesItem : Readable {
	default {
		RItem.Classname "CodesPickup";
		Readable.MenuName "CodesMenu";
		Inventory.Icon "SCRLA0";
		tag "Code rotation";
	}
}

class CodesMenu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("codes");
		BookPage page = new ("BookPage");
		page.lines.push("During development, code is \cA9999\cJ.");
		contents.pages.push(page);
		return contents;
	}
}