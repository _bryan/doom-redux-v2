class CopperSheetPickup : RPickup {
	default {
		RPickup.Classname "CopperSheetItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cBCopper [Cu 29]";
	}
	states {
		spawn:
			CPSH A -1;
			Stop;
	}
}

class CopperSheetItem : RChemical {
	default {
		RItem.Classname "CopperSheetPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "ZNSHA0";
		Tag "\cBCopper [Cu 29]";
	}
}