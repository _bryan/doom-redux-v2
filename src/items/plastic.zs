class PlasticPickup : RPickup {
	default {
		RPickup.Classname "PlasticItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "Plastic";
	}
	states {
		spawn:
			PLST A -1;
			Stop;
	}
}

class PlasticItem : RChemical {
	default {
		RItem.Classname "PlasticPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "PLSTA0";
		Tag "Plastic";
	}
}