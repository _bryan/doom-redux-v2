class PaperPickup : RPickup {
	default {
		RPickup.Classname "PaperItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cOPaper";
	}
	states {
		spawn:
			PAPR A -1;
			Stop;
	}
}

class PaperItem : RChemical {
	default {
		RItem.Classname "PaperPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "PAPRA0";
		Tag "\cOPaper";
	}
}