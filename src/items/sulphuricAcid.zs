class SulphuricAcidPickup : RPickup {
	default {
		RPickup.Classname "SulphuricAcidItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cJSulphuric acid";
	}
	states {
		spawn:
			SULA A -1;
			Stop;
	}
}

class SulphuricAcidItem : RChemical {
	default {
		RItem.Classname "SulphuricAcidPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "SULAA0";
		Tag "\cJH2SO4";
	}
}