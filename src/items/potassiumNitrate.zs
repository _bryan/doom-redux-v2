class PotassiumNitratePickup : RPickup {
	default {
		RPickup.Classname "PotassiumNitrateItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cPPotassium Nitrate";
	}
	states {
		spawn:
			PTNT A -1;
			Stop;
	}
}

class PotassiumNitrateItem : RChemical {
	default {
		RItem.Classname "PotassiumNitratePickup";
		Inventory.Icon "PTNTA0";
		tag "\cPKNO3";
	}
}