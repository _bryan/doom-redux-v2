
class ChromeAlumPickup : RPickup {
	default {
		RPickup.Classname "ChromeAlumItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cTChrome alum";
	}
	states {
		spawn:
			CHAM A -1;
			Stop;
	}
}

class ChromeAlumItem : RChemical {
	default {
		RItem.Classname "ChromeAlumPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "CHAMA0";
		Tag "\cTKCr(SO4)2";
	}
}