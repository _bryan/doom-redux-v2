
class SodiumPickup : RPickup {
	default {
		RPickup.Classname "SodiumItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cJSodium";
	}
	states {
		spawn:
			SODI A -1;
			Stop;
	}
}

class SodiumItem : RChemical {
	default {
		RItem.Classname "SodiumPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "SODIA0";
		Tag "\cJSodium [Na 11]";
	}
}