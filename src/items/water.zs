
class WaterPickup : RPickup {
	default {
		RPickup.Classname "WaterItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cNWater";
	}
	states {
		spawn:
			BCMA A -1;
			Stop;
	}
}

class WaterItem : RChemical {
	default {
		RItem.Classname "WaterPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "BCMAA0";
		Tag "\cNH2O";
	}
}