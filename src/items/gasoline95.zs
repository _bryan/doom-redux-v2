
class Gasoline95Pickup : RPickup {
	default {
		RPickup.Classname "Gasoline95Item";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cEGasoline [95]";
	}
	states {
		spawn:
			GASO A -1;
			Stop;
	}
}

class Gasoline95Item : RItem {
	default {
		RItem.Classname "Gasoline95Pickup";
		Inventory.Icon "GASOA0";
		tag "\cEGasoline (95)";
	}
}