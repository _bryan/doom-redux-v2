
class WoodPickup : RPickup {
	default {
		RPickup.Classname "WoodItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cSWood";
	}
	states {
		spawn:
			WOOD A -1;
			Stop;
	}
}

class WoodItem : RChemical {
	default {
		RItem.Classname "WoodPickup";
		Inventory.MaxAmount 99;
		Inventory.Icon "WOODA0";
		Tag "\cSWood";
	}
}