class RepeatedExcusePickup : RPickup {
	default {
		RPickup.Classname "RepeatedExcuseItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "ONSCHULD, EN TOE-GIFT";
		Scale 0.5;
	}
	states {
		spawn:
			SCRL A -1;
			Stop;
	}
}

class RepeatedExcuseItem : Readable {
	default {
		RItem.Classname "RepeatedExcusePickup";
		Readable.MenuName "RepeatedExcuseMenu";
		Inventory.Icon "SCRLA0";
		tag "ONSCHULD, EN TOE-GIFT";
	}
}

class RepeatedExcuseMenu : BookMenu {
	override BookContents createContents() {
		BookContents contents = createBook("rpex");
		return contents;
	}
}