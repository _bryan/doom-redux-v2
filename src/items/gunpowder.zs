class GunpowderPickup : RPickup {
	default {
		RPickup.Classname "GunpowderItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cMGunpowder";
	}
	states {
		spawn:
			GNPW A -1;
			Stop;
	}
}

class GunpowderItem : RChemical {
	default {
		RItem.Classname "GunpowderPickup";
		Inventory.Icon "GNPWA0";
		tag "\cMGunpowder";
	}
}