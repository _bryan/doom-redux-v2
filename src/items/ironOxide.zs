
class IronOxidePickup : RPickup {
	default {
		RPickup.Classname "IronOxideItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cMIron Oxide";
	}
	states {
		spawn:
			IROX A -1;
			Stop;
	}
}

class IronOxideItem : RChemical {
	default {
		RItem.Classname "IronOxidePickup";
		Inventory.Icon "IROXA0";
		tag "\cMFe3O4";
	}
}