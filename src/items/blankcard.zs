class BlankCardPickup : RPickup {
	default {
		RPickup.Classname "BlankCardItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cOBlank card";
	}
	states {
		spawn:
			BLCR A -1;
			Stop;
	}
}

class BlankCardItem : RItem {
	default {
		RItem.Classname "BlankCardPickup";
		-Inventory.invbar
		Inventory.MaxAmount 99;
		Inventory.Icon "BLCRA0";
		Tag "\cOBlank card";
	}
}

class CorrectCardPickup : RPickup {
	default {
		RPickup.Classname "CorrectCardItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cBCard";
	}
	states {
		spawn:
			BLCR A -1;
			Stop;
	}
}

class CorrectCardItem : RItem {
	default {
		-Inventory.invbar
		RItem.Classname "CorrectCardPickup";
		Inventory.MaxAmount 1;
		Inventory.Icon "BLCRA0";
		Tag "\cBCard";
	}
}

class IncorrectCardPickup : RPickup {
	default {
		RPickup.Classname "IncorrectCardItem";
		RPickup.Amount 1;
		RPickup.Sound "sfx/glass/glass";
		Tag "\cACard";
	}
	states {
		spawn:
			BLCR A -1;
			Stop;
	}
}

class IncorrectCardItem : RItem {
	default {
		RItem.Classname "IncorrectCardPickup";
		Inventory.MaxAmount 1;
		Inventory.Icon "BLCRA0";
		Tag "\cACard";
	}
}
