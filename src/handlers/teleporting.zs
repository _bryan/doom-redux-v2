
class TeleportHandler : EventHandler {
	
	SaveTypewriter lastUsed;

	Teleporter gA;
	Teleporter gB;

	Teleporter bA;
	Teleporter bB;

	Teleporter pA;
	Teleporter pB;

	bool done;

	override void WorldTick() {
		if (!done) {

			Teleporter pt = Teleporter(Actor.Spawn("Teleporter"));
			pt.SetOrigin((-1508, 1653, 0), false);
			pt.SetStateLabel("purple");
			pt.color = "purple";
			pt.crystalSet = true;
			pA = pt;
			done = true;
		}
	}

}