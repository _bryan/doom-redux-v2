
class KeycardHandler : EventHandler {
	
	String secretCode;
	bool scriptOpened[8];

	override void WorldLoaded (WorldEvent e) {
		super.WorldLoaded(e);
		secretCode = "9999";
	}

	bool checkCode(String code) {
		return code == secretCode;
	}

	override void NetworkProcess (ConsoleEvent e) {
		Array<string> command;
		e.Name.Split (command, ":");
		String cmd = command[0];
		if (cmd == "codecard") {
			String code = command[1];
			players[e.player].mo.TakeInventory("BlankCardItem", 1);
			if (checkCode(code)) {
				players[e.player].mo.GiveInventory("CorrectCardItem", 1);
			}
			else {
				players[e.player].mo.GiveInventory("IncorrectCardItem", 1);
			}
			console.printf("Card encoded.");
		}
	}

}