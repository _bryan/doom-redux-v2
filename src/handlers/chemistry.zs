
class ChemistryHandler : EventHandler {

	Array<ChemistryRecipe> unlocked;
	Array<ChemistryRecipe> recipes;
	CoffeeMachine beingUsed[8];

	override void WorldLoaded (WorldEvent e) {
		super.WorldLoaded(e);
		initializeRecipies();
	}

	ChemistryRecipe findRecipe(String n) {
		for(int i = 0; i < unlocked.size(); i++) {
			ChemistryRecipe next = unlocked[i];
			if (next.name == n) {
				return next;
			}
		}
		return NULL;
	}

	override void NetworkProcess (ConsoleEvent e) {
		Array<string> command;
		e.Name.Split (command, ":");
		String cmd = command[0];
		if (cmd == "brewitem") {
			String cls = command[1];
			CoffeeMachine m = beingUsed[e.Player];
			m.busy = true;
			m.crafting = findRecipe(cls);
			m.currentTime = m.crafting.timecost;
			for (int i = 0; i < m.crafting.requires.size(); i++) {
				ChemistryRequirement r = m.crafting.requires[i];
				players[e.Player].mo.TakeInventory(r.classname, r.amount);
			}
			m.SetStateLabel("cooking");
		}
	}

	CoffeeMachine getMachine(int id) const {
		return beingUsed[id];
	}

	bool hasRecipe(string recipe) {
		bool has = false;
		for (int i = 0; i < unlocked.size(); i++) {
			ChemistryRecipe next = unlocked[i];
			if (next.name == recipe) {
				return true;
			}
		}
		return has;
	}

	ui void canCraft(Actor a, Array<ChemistryRecipe> craftable) {
		for(int i = 0; i < unlocked.size(); i++) {
			ChemistryRecipe next = unlocked[i];
			bool can = next.canCraft(a);
			//if (can) {
				craftable.push(next);
			//}
		}
	}

	private void initializeRecipies() {

		
		ChemistryRecipe mohr = new ("ChemistryRecipe");
		mohr.name = "Green crystal";
		mohr.timecost = (35 * 10);
		mohr.result = "GreenCrystalPickup";
		mohr.amount = 1;
		mohr.addRequirement("AmmoniumSulfateItem", 1);
		mohr.addRequirement("IronSulfateItem", 1);
		mohr.addRequirement("WaterItem", 3);
		mohr.colors.push("#31ac33");
		mohr.colors.push("#2840f0");
		mohr.colors.push("#979797");
		mohr.descr.push("Synchronizes location.");
		mohr.descr.push("Permanent.");
		recipes.push(mohr);
		unlocked.push(mohr);

		ChemistryRecipe deepblue = new ("ChemistryRecipe");
		deepblue.name = "Blue crystal";
		deepblue.timecost = (35 * 5);
		deepblue.result = "BlueCrystalPickup";
		deepblue.amount = 1;
		deepblue.addRequirement("AmmoniumSulfateItem", 1);
		deepblue.addRequirement("CopperSulfateItem", 1);
		deepblue.addRequirement("WaterItem", 3);
		deepblue.colors.push("#0034c9");
		deepblue.colors.push("#2840f0");
		deepblue.descr.push("Synchronizes location.");
		deepblue.descr.push("Single use.");
		recipes.push(deepblue);
		unlocked.push(deepblue);

		ChemistryRecipe escape = new ("ChemistryRecipe");
		escape.name = "White crystal";
		escape.timecost = (35 * 5);
		escape.result = "WhiteCrystalPickup";
		escape.amount = 1;
		escape.addRequirement("AmmoniumSulfateItem", 1);
		escape.addRequirement("ZincSulfateItem", 1);
		escape.addRequirement("WaterItem", 3);
		escape.colors.push("#2840f0");
		escape.colors.push("#cccccc");
		escape.descr.push("Synchronized to last");
		escape.descr.push("typewriter used.");
		escape.descr.push("Single use.");
		recipes.push(escape);
		unlocked.push(escape);

		ChemistryRecipe secretCrystal = new ("ChemistryRecipe");
		secretCrystal.name = "Purple Crystal";
		secretCrystal.timecost = (35 * 5);
		secretCrystal.result = "PurpleCrystalPickup";
		secretCrystal.amount = 1;
		secretCrystal.addRequirement("AmmoniumSulfateItem", 1);
		secretCrystal.addRequirement("ChromeAlumItem", 1);
		secretCrystal.addRequirement("WaterItem", 3);
		secretCrystal.colors.push("#cccccc");
		secretCrystal.colors.push("#bd19e0");
		secretCrystal.colors.push("#2840f0");
		secretCrystal.descr.push("Pre-synchronized to");
		secretCrystal.descr.push("location. Single use.");
		recipes.push(secretCrystal);
		unlocked.push(secretCrystal);

		ChemistryRecipe convertCell = new ("ChemistryRecipe");
		convertCell.name = "Convert to cell";
		convertCell.timecost = (35 * 5);
		convertCell.result = "Cell";
		convertCell.amount = 5;
		convertCell.addRequirement("BatteryItem", 1);
		convertCell.colors.push("#00cc00");
		convertCell.descr.push("Converts battery to");
		convertCell.descr.push("vanilla doom cell.");
		recipes.push(convertCell);
		unlocked.push(convertCell);

		ChemistryRecipe convertBattery = new ("ChemistryRecipe");
		convertBattery.name = "Convert to battery";
		convertBattery.timecost = (35 * 5);
		convertBattery.result = "BatteryPickup";
		convertBattery.amount = 1;
		convertBattery.addRequirement("Cell", 10);
		convertBattery.colors.push("#00cc00");
		convertBattery.descr.push("Convert cell to");
		convertBattery.descr.push("battery.");
		recipes.push(convertBattery);
		unlocked.push(convertBattery);

		ChemistryRecipe convertPlastic = new ("ChemistryRecipe");
		convertPlastic.name = "Convert to plastic";
		convertPlastic.timecost = (35 * 5);
		convertPlastic.result = "PlasticPickup";
		convertPlastic.amount = 5;
		convertPlastic.addRequirement("Gasoline95Item", 1);
		convertPlastic.colors.push("#d4c30c");
		convertPlastic.colors.push("#d4c30c");
		convertPlastic.descr.push("Convert gas to");
		convertPlastic.descr.push("plastic.");
		recipes.push(convertPlastic);
		unlocked.push(convertPlastic);

		ChemistryRecipe createBattery = new ("ChemistryRecipe");
		createBattery.name = "Create battery";
		createBattery.timecost = (35 * 5);
		createBattery.result = "BatteryPickup";
		createBattery.amount = 1;
		createBattery.addRequirement("ZincSheetItem", 3);
		createBattery.addRequirement("CopperSheetItem", 3);
		createBattery.addRequirement("PaperItem", 3);
		createBattery.addRequirement("SulphuricAcidItem", 1);
		createBattery.colors.push("#2840f0");
		createBattery.colors.push("#0034c9");
		createBattery.colors.push("#d4c30c");
		createBattery.descr.push("Create a new");
		createBattery.descr.push("battery.");
		recipes.push(createBattery);
		unlocked.push(createBattery);

		ChemistryRecipe blankCard = new ("ChemistryRecipe");
		blankCard.name = "Create blank card";
		blankCard.timecost = (35 * 5);
		blankCard.result = "BlankCardPickup";
		blankCard.amount = 1;
		blankCard.addRequirement("PlasticItem", 1);
		blankCard.addRequirement("IronOxideItem", 1);
		blankCard.colors.push("#2840f0");
		blankCard.colors.push("#0034c9");
		blankCard.descr.push("Create a blank");
		blankCard.descr.push("card. Code needs");
		blankCard.descr.push("to be set.");
		recipes.push(blankCard);
		unlocked.push(blankCard);

		ChemistryRecipe charcoal = new ("ChemistryRecipe");
		charcoal.name = "Make charcoal";
		charcoal.timecost = (35 * 5);
		charcoal.result = "CharcoalPickup";
		charcoal.amount = 5;
		charcoal.addRequirement("WoodItem", 1);
		charcoal.colors.push("#2840f0");
		charcoal.descr.push("Convert wood to");
		charcoal.descr.push("charcoal.");
		recipes.push(charcoal);
		unlocked.push(charcoal);

		ChemistryRecipe makepaper = new ("ChemistryRecipe");
		makepaper.name = "Make paper";
		makepaper.timecost = (35 * 5);
		makepaper.result = "PaperPickup";
		makepaper.amount = 30;
		makepaper.addRequirement("WoodItem", 1);
		makepaper.addRequirement("WaterItem", 1);
		makepaper.colors.push("#2840f0");
		makepaper.descr.push("Convert wood to");
		makepaper.descr.push("paper.");
		recipes.push(makepaper);
		unlocked.push(makepaper);

		ChemistryRecipe mixgunpow = new ("ChemistryRecipe");
		mixgunpow.name = "Mix gunpowder";
		mixgunpow.timecost = (35 * 5);
		mixgunpow.result = "GunpowderPickup";
		mixgunpow.amount = 10;
		mixgunpow.addRequirement("PotassiumNitrateItem", 1);
		mixgunpow.addRequirement("SulfurItem", 1);
		mixgunpow.addRequirement("CharcoalItem", 1);
		mixgunpow.colors.push("#2840f0");
		mixgunpow.colors.push("#0034c9");
		mixgunpow.descr.push("Mix gunpowder for");
		mixgunpow.descr.push("making ammo.");
		recipes.push(mixgunpow);
		unlocked.push(mixgunpow);

		ChemistryRecipe synthWater = new ("ChemistryRecipe");
		synthWater.name = "Synthesize H2O";
		synthWater.timecost = (35 * 10);
		synthWater.result = "WaterPickup";
		synthWater.amount = 1;
		synthWater.addRequirement("HydrogenItem", 1);
		synthWater.addRequirement("OxygenItem", 2);
		synthWater.addRequirement("BatteryItem", 3);
		synthWater.colors.push("#0000cc");
		synthWater.descr.push("Combine hydrogen and");
		synthWater.descr.push("oxygen.");
		recipes.push(synthWater);
		unlocked.push(synthWater);

		ChemistryRecipe synthAcid1 = new ("ChemistryRecipe");
		synthAcid1.name = "Synthesize H2SO4";
		synthAcid1.timecost = (35 * 10);
		synthAcid1.result = "SulphuricAcidPickup";
		synthAcid1.amount = 1;
		synthAcid1.addRequirement("WaterItem", 3);
		synthAcid1.addRequirement("SulfurItem", 1);
		synthAcid1.addRequirement("OxygenItem", 1);
		synthAcid1.colors.push("#0000cc");
		synthAcid1.colors.push("#cccccc");
		synthAcid1.colors.push("#d4c30c");
		synthAcid1.descr.push("Synthesize sulphuric");
		synthAcid1.descr.push("acid.");
		recipes.push(synthAcid1);
		unlocked.push(synthAcid1);

		ChemistryRecipe synthCS = new ("ChemistryRecipe");
		synthCS.name = "Synthesize CuSO4";
		synthCS.timecost = (35 * 10);
		synthCS.result = "CopperSulfateItem";
		synthCS.amount = 1;
		synthCS.addRequirement("CopperSheetItem", 1);
		synthCS.addRequirement("SulphuricAcidItem", 3);
		synthCS.addRequirement("SodiumItem", 3);
		synthCS.addRequirement("BatteryItem", 1);
		synthCS.colors.push("#efb869");
		synthCS.colors.push("#d4c30c");
		synthCS.colors.push("#0000cc");
		synthCS.descr.push("Synthesize copper");
		synthCS.descr.push("sulfate.");
		recipes.push(synthCS);
		unlocked.push(synthCS);

		ChemistryRecipe ironox = new ("ChemistryRecipe");
		ironox.name = "Synthesize Fe3O4";
		ironox.timecost = (35 * 10);
		ironox.result = "IronOxideItem";
		ironox.amount = 1;
		ironox.addRequirement("IronSulfateItem", 1);
		ironox.addRequirement("HydrochloricAcidItem", 3);
		ironox.addRequirement("SodiumItem", 3);
		ironox.addRequirement("WaterItem", 15);
		ironox.colors.push("#2840f0");
		ironox.colors.push("#d4c30c");
		ironox.colors.push("#cccccc");
		ironox.descr.push("Synthesize iron");
		ironox.descr.push("oxide.");
		recipes.push(ironox);
		unlocked.push(ironox);

		ChemistryRecipe synthZN = new ("ChemistryRecipe");
		synthZN.name = "Synthesize ZnSO4";
		synthZN.timecost = (35 * 10);
		synthZN.result = "ZincSulfateItem";
		synthZN.amount = 1;
		synthZN.addRequirement("ZincSheetItem", 1);
		synthZN.addRequirement("SulphuricAcidItem", 3);
		synthZN.addRequirement("SodiumItem", 3);
		synthZN.addRequirement("BatteryItem", 1);
		synthZN.colors.push("#cccccc");
		synthZN.colors.push("#d4c30c");
		synthZN.colors.push("#0000cc");
		synthZN.descr.push("Synthesize zinc");
		synthZN.descr.push("sulfate.");
		recipes.push(synthZN);
		unlocked.push(synthZN);
				
	}

}