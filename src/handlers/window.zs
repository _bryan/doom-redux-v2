
class WindowHandler : EventHandler {
	
	override void WorldLoaded (WorldEvent e) {
		super.WorldLoaded(e);
		if (sv_window_visibility == 0) {
			LineIdIterator lines = Level.CreateLineIdIterator(409);
			while(true) {
				int lineid = lines.next();
				if (lineid == -1) {
					break;
				}
				level.lines[lineid].flags += Line.ML_BLOCKSIGHT;
			}
		}
	}
	
}