class BookMenu : GenericMenu {

	virtual String background() {
		return "";
	}

	int fadeTime;
	int pageFade;
	int pagen;
	Font fnt;
	Font sml;



	override void Init (Menu parent) {
		Super.Init(parent);
		fadeTime = 0;
		pageFade = 0;
		pagen = 0;
		fnt = Font.GetFont("Q2SMFNNM");
		sml = Font.GetFont("SMALLFONT");
		DontDim = True;
		DontBlur = True;
		menuactive = Menu.OnNoPause;
		MenuSound("sfx/book/open");
	}

	override bool MenuEvent (int mkey, bool fromController) {
		int pageCount = getBook().pages.size();
		if (mkey == MKEY_RIGHT) {
			if (pageCount > 1 && pageCount - 1 != pagen) {
				MenuSound("sfx/book/flip");
				pagen++;
				pageFade = 0;
			}
		}
		else if (mkey == MKEY_LEFT) {
			if (pageCount > 1 && pagen != 0) {
				MenuSound("sfx/book/flip");
				pagen--;
				pageFade = 0;
			}
		}
		else if (mkey == MKEY_BACK) {
			MenuSound("sfx/book/close");
			close();
		}
		return false;
	}

	String colorCode(String input) {
		String output = input;
		output.Replace("\\cA", "\cA");
		output.replace("\\cB", "\cB");
		output.replace("\\cT", "\cT");
		output.replace("\\cU", "\cU");
		output.replace("\\cQ", "\cQ");
		output.replace("\\cJ", "\cJ");
		output.replace("\\cN", "\cN");
		return output;
	}

	override void Drawer() {
		BookContents book = getBook();
		if (book) {
			BookPage page = book.pages[pagen];
			double alpha = clamp(fadeTime / (35.0 * 3.0), 0.0, 1.0);
			double pageAlpha = clamp(pageFade / (35.0 * 3.0), 0.0, 1.0);
			
			let tex = TexMan.checkForTexture("textures/bc_blackframe1.png", TexMan.Type_Any);
			screen.DrawTexture(tex, false, -70, 0, DTA_320x200, 1, DTA_Alpha, alpha / 1.2, DTA_DestWidth, 460, DTA_DestHeight, 200);

			if (background() != "") {
				let tex2 = TexMan.checkForTexture(background(), TexMan.Type_Any);
				screen.DrawTexture(tex2, false, -0, 0, DTA_320x200, 1, DTA_Alpha, alpha / 3.0, DTA_DestWidth, 320, DTA_DestHeight, 200);
			}

			let tex3 = TexMan.checkForTexture("graphics/covers/bookdark.png", TexMan.Type_Any);
			screen.DrawTexture(tex3, false, -70, 0, DTA_320x200, 1, DTA_Alpha, alpha, DTA_DestWidth, 460, DTA_DestHeight, 200);

			int height = page.lines.size() * 8;
			int width = 0;
			for (int l = 0; l < page.lines.size(); l++) {
				int lwidth = fnt.StringWidth(colorCode(page.lines[l]));
				if (lwidth > width) {
					width = lwidth;
				}
			}

			int yoff = ((200 - height) / 2) - 10;
			for (int l = 0; l < page.lines.size(); l++) {
				String line = colorCode(page.lines[l]);
				screen.DrawText(fnt, Font.CR_WHITE, (320 - width) / 2, (l * 8) + yoff, line, DTA_320x200, 1, DTA_Alpha, pageAlpha);
			}
			fadeTime = clamp(fadeTime + 1, 0, 105);
			pageFade = clamp(pageFade + 1, 0, 105);

			String pagestr = String.format("%i / %i", pagen + 1, book.pages.size());
			int wpagestr = sml.StringWidth(pagestr);
			int pagex = (320 - wpagestr) / 2;
			screen.DrawText(sml, Font.CR_YELLOW, pagex, 180, pagestr, DTA_320x200, 1, DTA_Alpha, alpha);

			page.CustomDraw(sml, alpha);


			if (book.pages.size() > 1 && pagen > 0) {
				screen.DrawText(sml, Font.CR_RED, 105, 180, "<", DTA_320x200, 1, DTA_Alpha, alpha);
			}
			
			if (book.pages.size() > 1 && pagen == book.pages.size() - 1) {
				screen.DrawText(sml, Font.CR_White, 210, 180, "*", DTA_320x200, 1, DTA_Alpha, alpha);
			}
			else if (book.pages.size() > 1 && pagen >= 0) {
				screen.DrawText(sml, Font.CR_RED, 210, 180, ">", DTA_320x200, 1, DTA_Alpha, alpha);
			}
			else if (book.pages.size() == 1) {
				screen.DrawText(sml, Font.CR_RED, 210, 180, "*", DTA_320x200, 1, DTA_Alpha, alpha);
			}
		}
	}

	BookPage createPageFromLump(String name) {
		BookPage p = new("BookPage");
		int lump = -1;
		while (-1 != (lump = Wads.FindLump(name, lump + 1))) {
			p.lines.push(Wads.ReadLump(lump));
		}
		return p;
	}

	BookContents createBook(String filename) {
		BookContents contents = new ("BookContents");
		BookPage page = new ("BookPage");
		Array<String> buffer;
		int lump = -1;
		while (-1 != (lump = Wads.FindLump(filename, lump + 1))) {
			String content = Wads.ReadLump(lump);
			Array<String> fileLines;
			content.Split(fileLines, "\n");
			for (int l = 0; l < fileLines.size(); l++) {
				String line = fileLines[l];
				if (line.IndexOf("^^^") > -1) {
					contents.pages.push(page);
					page = new ("BookPage");
				}
				else {
					page.lines.push(line);
				}
			}
		}
		return contents;
	}

	BookContents _contents;
	BookContents getBook() {
		if (!_contents) {
			_contents = createContents();
		}
		return _contents;
	}

	virtual BookContents createContents() {
		return null;
	}

}