
class CardWriterMenu : BMenu {

	KeycardHandler hndl;

	Font fnt;
	PlayerInfo info;

	TextureID bg;
	TextureID nixiybg;
	TextureID nixiyfG;
	TextureID card;
	TextureID nixiyNumbers[10];
	TextureID cardlight;

	int cursor;
	int values[4];
	bool cardInserted;

	int timeShowError;

	override void Drawer() {
		super.Drawer();
		if (!hndl.scriptOpened[consoleplayer]) {
			self.close();
		}
				
		drawBackground(bg, 1.0);
		int nixiyStartX = 88;
		for (int i = 0; i < 4; i++) {
			drawNixiy(
				nixiyStartX + (i * 32),
				5,
				values[i]
			);
		}
		drawNumpad();

		if (cardInserted) {
			drawTex(card, 100, 73);
			drawTex(cardlight, 143, 95);
		}
	}

	override void Init (Menu parent) {
		super.Init(parent);
		hndl = KeycardHandler(EventHandler.Find("KeycardHandler"));

		info = players[consoleplayer];
		fnt = Font.GetFont("CONFONT");
		DontDim = False;
		menuactive = Menu.OnNoPause;
		cursor = 10;
		for (int i = 0; i < 4; i++) {
			values[i] = -1;
		}

		bg = getTex("graphics/cardwriter/bg.png");
		nixiybg = getTex("graphics/cardwriter/nixiybg.png");
		nixiyfg = getTex("graphics/cardwriter/nixiyfg.png");
		card = getTex("graphics/cardwriter/card.png");
		cardlight = getTex("graphics/cardwriter/ledcard1.png");
		for (int i = 0; i < 10; i++) {
			string img = string.format("graphics/cardwriter/%i.png", i);
			nixiyNumbers[i] = getTex(img);
		}

		
	}

	override bool MenuEvent (int mkey, bool fromController) {
		if (mkey == 0) {
			if (cursor == 0) {
				cursor = 1;
			}
			else if (cursor >= 1 && cursor <= 6) {
				cursor += 3;
			}
			else if (cursor == 10 || cursor == 11) {
				cursor = cursor == 10 ? 11 : 10;
			}

		}

		else if (mkey == 1) {
			if (cursor >= 4 && cursor <= 9) {
				cursor -= 3;
			}
			else if (cursor >= 1 && cursor <= 3) {
				cursor = 0;
			}
			else if (cursor == 10 || cursor == 11) {
				cursor = cursor == 10 ? 11 : 10;
			}

		}

		else if (mkey == 2) {
			if (cursor == 3 ||
				cursor == 6 ||
				cursor == 9 ||
				cursor == 2 ||
				cursor == 5 ||
				cursor == 8) {
				cursor--;
			}
			else if (cursor == 10 || cursor == 11) {
				cursor = 6;
			}
		}

		else if (mkey == 3) {
			if (cursor == 1 ||
				cursor == 2 ||
				cursor == 4 ||
				cursor == 5 ||
				cursor == 7 ||
				cursor == 8) {
				cursor++;
			}
			else if (cursor == 3 || cursor == 6 || cursor == 9) {
				cursor = 10;
			}
		}

		else if (mkey == 6) {

			if (cursor == 10) {
				if (!cardInserted) {
					tryInsert();
				}
				else {
					MenuSound("card/no");
				}
			}

			else if (cursor == 11) {
				if (!cardInserted) {
					timeShowError = (35 * 5);
					MenuSound("card/no");
				}
				else if (!validInput()) {
					MenuSound("card/no");
				}
				else {
					MenuSound("card/press");
					String msg = string.format("codecard:%s", getCode());
					EventHandler.SendNetworkEvent(msg);
					self.close();
				}
			}
			
			else {
				bool hasCard = info.mo.CountInv("BlankCardItem");
				if (!cardInserted) {
					MenuSound("card/no");
				}
				else {
					MenuSound("card/press");
					setNextValue(cursor);
				}
			}
			
		}


		return super.MenuEvent(mkey, fromController);
	}

	private void drawNumpad() {
		int startx = 103;
		int starty = 115;

		int stepx = 16;
		int stepy = 16;

		writeln(startx,               starty,               cursor == 7 ? "\cF7" : "7", fnt);
		writeln(startx + stepx,       starty,               cursor == 8 ? "\cF8" : "8", fnt);
		writeln(startx + (stepx * 2), starty,               cursor == 9 ? "\cF9" : "9", fnt);
		writeln(startx,               starty + stepy,       cursor == 4 ? "\cF4" : "4", fnt);
		writeln(startx + stepx,       starty + stepy,       cursor == 5 ? "\cF5" : "5", fnt);
		writeln(startx + (stepx * 2), starty + stepy,       cursor == 6 ? "\cF6" : "6", fnt);
		writeln(startx,               starty + (2 * stepy), cursor == 1 ? "\cF1" : "1", fnt);
		writeln(startx + stepx,       starty + (2 * stepy), cursor == 2 ? "\cF2" : "2", fnt);
		writeln(startx + (stepx * 2), starty + (2 * stepy), cursor == 3 ? "\cF3" : "3", fnt);
		writeln(startx,               starty + (3 * stepy), cursor == 0 ? "\cF0" : "0", fnt);		
		writeln(178, 119, cursor == 10 ? "\cFINS" : "INS", fnt);
		writeln(180, 146, cursor == 11 ? "\cFSET" : "SET", fnt);

		if (timeShowError > 0) {
			writeln(96, 54, "\cANo blank card", fnt);
			timeShowError--;
		}

	}

	private void setNextValue(int v) {
		for(int i = 0; i < 4; i++) {
			int nv = values[i];
			if (nv == -1) {
				values[i] = v;
				break;
			}
		}
	}

	private void drawNixiy(int x, int y, int value) {
		drawTex(nixiybg, x, y);
		if (value > -1 && value < 10) {
			drawTex(nixiyNumbers[value], x, y);
		}
		drawTex(nixiyfg, x, y);
	}

	private void tryInsert() {
		bool hasCard = info.mo.CountInv("BlankCardItem");
		if (!hasCard) {
			timeShowError = 35 * 5;
			MenuSound("card/no");
		}
		else {
			cardInserted = true;
			MenuSound("card/press");
		}
	}

	private String getCode() {
		String code = "";
		for (int i = 0; i < 4; i++) {
			code = string.format("%s%i", code, values[i]);
		}
		return code;
	}

	private bool validInput() {
		bool valid = true;
		for (int i = 0; i < 4; i++) {
			if (values[i] == -1) {
				valid = false;
				break;
			}
		}
		return valid;
	}

}