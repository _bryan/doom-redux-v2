class BUI {
	ui void writeln(int x, int y, string msg, font f) {
		screen.drawText(f, font.cr_white, x, y, msg, DTA_320x200, 1);
	}


}

class BUIList : BUI {
	int page;
	int lines;
	int lineHeight;
}

class BMenu : GenericMenu {

	TextureID blackFill;

	ui void writeln(int x, int y, string msg, font f) {
		screen.drawText(f, font.cr_white, x, y, msg, DTA_320x200, 1);
	}

	TextureID getTex(String name) {
		return TexMan.CheckForTexture(name, TexMan.Type_Any);
	}

	ui void drawfillBlack(float alpha=1.0) {
		screen.drawTexture(blackFill, false, 160, 100,
			DTA_320x200, 1,
			DTA_CenterOffset, 1,
			DTA_Alpha, alpha,
			DTA_DestWidth, 5000,
			DTA_DestHeight, 5000
		);
	}

	ui void drawBackground(TextureID tex, float alpha=1.0) {
		screen.drawTexture(tex, false, 160, 100, 
			DTA_320x200, 1,
			DTA_CenterOffset, 1,
			DTA_Alpha, alpha
		);
	}

	ui void drawTex(TextureID tex, int x, int y, float alpha=1.0) {
		screen.drawTexture(tex, false, x, y, 
			DTA_320x200, 1, 
			DTA_Alpha, alpha
		);
	}

	override void Init (Menu parent) {
		super.Init(parent);
		blackFill = getTex("textures/bc_black1.png");
	}
}