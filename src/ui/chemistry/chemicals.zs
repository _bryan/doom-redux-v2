class ChemicalList : BUIList {
	Array<Class<Inventory> > items;
	int getPageCount() {
		double t = items.size() / lines;
		double o = (items.size() % lines);
		if (o > 0) {
			t++;
		}
		return ceil(t);
	}
	
	void getPageList(int page, Array<Class<Inventory> > lst) {
		int startIndex = (page * lines);
		for (int i = startIndex; i < startIndex + lines; i++) {
			if (i >= items.size()) {
				break;
			}
			lst.push(items[i]);
		}
	}

	int getPageListCount(int page) {
		int startIndex = (page * lines);
		int count = 0;
		for (int i = startIndex; i < startIndex + lines; i++) {
			if (i >= items.size()) {
				break;
			}
			count++;
		}
		return count;
	}

	void movePage(int dir) {
		int newPage = page + dir;
		if (newPage >= getPageCount()) {
			newPage = 0;
		}
		else if (newPage < 0) {
			newPage = getPageCount() - 1;
		}
		page = newPage;
	}

	ui void draw(Font fnt, PlayerInfo info) {
		int x = 0;
		int y = 10;
		Array<Class<Inventory> > pageItems;
		getPageList(page, pageItems);
		for (int i = 0; i < pageItems.size(); i++) {
			let defaults = GetDefaultByType(pageItems[i]);
			String tag = defaults.getTag();
			int amt = info.mo.CountInv(pageItems[i]);
			String amtString = string.format("\cD%i", amt);
			if (amt == 0) {
				amtString = string.format("\cG%i", amt);
			}
			writeln(x, y + (i * lineHeight), amtString, fnt);
			writeln(20, y + (i * lineHeight), tag, fnt);
		}
		string pg = string.format("Page %i / %i", page + 1, getPageCount());
		writeln(50, y + ((lines) * lineHeight) + 4, pg, fnt);
	}
}