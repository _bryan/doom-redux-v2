class RequiredList : BUIList {

	ChemistryRecipe selected;

	ui void draw(Font fnt, PlayerInfo info) {
		int x = 0;
		int y = 110;

		for (int i = 0; i < selected.requires.size(); i++) {
			ChemistryRequirement rq = selected.requires[i];
			Class<Inventory> cls = rq.classname;
			let defaults = GetDefaultByType(cls);

			String clr = "\cA";
			int holding = info.mo.countinv(cls);
			if (holding >= rq.amount) {
				clr = "\cD";
			}

			String msg = string.format("%s%i", clr, rq.amount);
			writeln(x, y + (i * lineHeight), msg, fnt);
			writeln(x + 20, y + (i * lineHeight), defaults.getTag(), fnt);
		}

	}
}