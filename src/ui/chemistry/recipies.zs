class RecipeList : BUIList {
	Array<ChemistryRecipe> items;
	int cursor;

	int getPageCount() {
		double t = items.size() / lines;
		double o = (items.size() % lines);
		if (o > 0) {
			t++;
		}
		return ceil(t);
	}

	ChemistryRecipe getSelected() {
		Array<ChemistryRecipe> pageItems;
		getPageList(pageItems);
		return pageItems[cursor];
	}

	void getPageList(Array<ChemistryRecipe> lst) {
		int startIndex = (page * lines);
		for (int i = startIndex; i < startIndex + lines; i++) {
			if (i >= items.size()) {
				break;
			}
			lst.push(items[i]);
		}
	}

	int getPageListCount() {
		int startIndex = (page * lines);
		int count = 0;
		for (int i = startIndex; i < startIndex + lines; i++) {
			if (i >= items.size()) {
				break;
			}
			count++;
		}
		return count;
	}

	void moveCursor(int dir) {
		int newPos = cursor + dir;
		if (newPos >= getPageListCount()) {
			movePage(1);
			newPos = 0;
		}
		else if (newPos < 0) {
			movePage(-1);
			newPos = getPageListCount() - 1;
		}
		cursor = newPos;
	}

	void movePage(int dir) {
		int newPage = page + dir;
		if (newPage >= getPageCount()) {
			newPage = 0;
		}
		else if (newPage < 0) {
			newPage = getPageCount() - 1;
		}
		page = newPage;
	}

	ui void draw(Font fnt, PlayerInfo info) {
		int x = 160;
		int y = 10;
		Array<ChemistryRecipe> pageItems;
		getPageList(pageItems);
		for (int i = 0; i < pageItems.size(); i++) {
			ChemistryRecipe r = pageitems[i];
			String nm = r.name;
			if (i == cursor) {
				nm = string.format("\cA%s", nm);
			}
			writeln(x, y + (i * lineHeight), nm, fnt);
		}
		string pg = string.format("Page %i / %i", page + 1, getPageCount());
		writeln(210, y + ((lines) * lineHeight) + 4, pg, fnt);

		ChemistryRecipe s = getSelected();
		int descy = 160;
		for (int i = 0; i < s.descr.size(); i++) {
			writeln(160, descy + (i * lineHeight), s.descr[i], fnt);
		}

	}
}