
class ChemistryMenu : GenericMenu {

	// public api - modders use these functions to add your recipes

	font smallfnt;
	Font fnt;
	ChemistryHandler hndl;
	PlayerInfo info;
	Array<ChemistryRecipe> craftable;
	CoffeeMachine machine;

	int cursor;
	int listoff;

	ChemicalList ui_chemlist;
	RecipeList   ui_reclist;
	RequiredList ui_reqlist;

	String active;

	int ct;
	int mt;

	override void Init (Menu parent) {
		super.Init(parent);
		info = players[consoleplayer];
		fnt = Font.GetFont("CONFONT");
		smallfnt = Font.GetFont("smallfont");
		DontDim = False;
		menuactive = Menu.OnNoPause;
		hndl = ChemistryHandler(EventHandler.Find("ChemistryHandler"));
		hndl.canCraft(info.mo, craftable);
		machine = hndl.getMachine(0);

		registerChemicals();
		active = "ChemicalList";

		ct = 0;
		mt = 15;

		registerRecipies();
		registerRequired();

		cursor = 0;
		listoff = 0;
	}

	private void registerRequired() {
		ui_reqlist = new ("RequiredList");
		ui_reqlist.lines = 6;
		ui_reqlist.lineHeight = 9;
		ui_reqlist.selected = ui_reclist.getSelected();
	}

	private void registerChemicals() {
		ui_chemlist = new ("ChemicalList");
		String names[23] = {
			"WaterItem",
			"WoodItem",
			"CharcoalItem",
			"PaperItem",
			"PlasticItem",
			"BatteryItem",
			"GunpowderItem",
			"Gasoline95Item",
			"AmmoniumSulfateItem",
			"ChromeAlumItem",
			"CopperSulfateItem",
			"ZincSulfateItem",
			"IronSulfateItem",
			"CopperSheetItem",
			"ZincSheetItem",
			"SulfurItem",
			"SodiumItem",
			"OxygenItem",
			"HydrogenItem",
			"SulphuricAcidItem",
			"HydrochloricAcidItem",
			"IronOxideItem",
			"PotassiumNitrateItem"
		};

		ui_chemlist.lines = 8;
		ui_chemlist.lineHeight = 9;
		for (int i = 0; i < 23; i++) {
			Class<Inventory> cast = names[i];
			ui_chemlist.items.push(cast);
		}
	}

	private void registerRecipies() {
		ui_reclist = new ("RecipeList");
		ui_reclist.lines = 14;
		ui_reclist.lineHeight = 9;
		ui_reclist.items.copy(hndl.unlocked);
	}

	override void Drawer() {
		super.Drawer();

		let bgtex1 = TexMan.checkForTexture("textures/bc_bgdebug.png", TexMan.Type_Any);
		let bgtex2 = TexMan.checkForTexture("textures/bc_bgdebug2.png", TexMan.Type_Any);
		let bgtex3 = TexMan.checkForTexture("textures/bc_bg2.png", TexMan.Type_Any);

		let bglabels = TexMan.checkForTexture("textures/bc_bglabels.png", TexMan.Type_Any);

		let acLabel = TexMan.checkForTexture("graphics/chemistry/chemicals_active.png");
		let rcLabel = TexMan.checkForTexture("graphics/chemistry/recipies_active.png");

		let bltex = TexMan.checkForTexture("textures/bc_black1.png", TexMan.Type_Any);
		screen.drawTexture(bltex, false, 160, 100, DTA_320x200, 1, DTA_CenterOffset, 1, DTA_Alpha, 0.8, DTA_DestWidth, 5000, DTA_DestHeight, 5000);
		

		if (machine.busy) {
			screen.drawTexture(bgtex3, false, 160, 100, DTA_320x200, 1, DTA_CenterOffset, 1, DTA_Alpha, 1.0);

			let busyLabel = TexMan.checkForTexture("graphics/chemistry/busy1.png");
			let bx = 160;
			let by = 10;

			ct++;
			if (ct == mt) {
				bx++;
				by++;
				ct = 0;
				busyLabel = TexMan.checkForTexture("graphics/chemistry/busy2.png");
			}

			screen.drawTexture(busyLabel, false, bx, by, DTA_320x200, 1);

			ChemistryRecipe r = machine.crafting;


		}
		else {
			screen.drawTexture(bgtex1, false, 160, 100, DTA_320x200, 1, DTA_CenterOffset, 1, DTA_Alpha, 1.0);
			screen.drawTexture(bglabels, false, 160, 100, DTA_320x200, 1, DTA_CenterOffset, 1, DTA_Alpha, 1.0);
			if (active == "ChemicalList") {
				screen.drawTexture(acLabel, false, -10, 0, DTA_320x200, 1);
			}
			else {
				screen.drawTexture(rcLabel, false, 159, 0, DTA_320x200, 1);
			}
			ui_reclist.draw(fnt, info);
			ui_reqlist.draw(fnt, info);

		}

		ui_chemlist.draw(fnt, info);
	}

	override bool MenuEvent (int mkey, bool fromController) {
		//console.printf("%i", mkey);

		if (mkey == 2 || mkey == 3 && !machine.busy) {
			active = ((active == "ChemicalList") ? "RecipeList" : "ChemicalList");
		}
		else if (mkey == 6) {
			AttemptCraft();
		}
		else if (active == "ChemicalList") {
			HandleChemicalList(mkey);
		}
		else if (active == "RecipeList" && !machine.busy) {
			HandleRecipeList(mkey);
		}

		return super.MenuEvent(mkey, fromController);
	}

	void AttemptCraft() {
		if (!machine.busy) {
			ChemistryRecipe r = ui_reclist.getSelected();

			bool canCraft = true;
			for (int i = 0; i < r.requires.size(); i++) {
				int amt = info.mo.countinv(r.requires[i].classname);
				if (amt < r.requires[i].amount) {
					canCraft = false;
					break;
				}
			}

			if (!canCraft) {
				MenuSound("coffee/no");
				return;
			}

			MenuSound("coffee/yes");
			active = "ChemicalList";
			String msg = string.format("brewitem:%s", r.name);
			EventHandler.SendNetworkEvent(msg);
		}
	}

	void checkCursor() {
		int items = ui_reclist.getPageListCount();
		if (ui_reclist.cursor >= items) {
			ui_reclist.cursor = items - 1;
		}
	}

	void HandleRecipeList(int mkey) {
		if (mkey == 5) {
			ui_reclist.movePage(1);
			checkCursor();
		}
		else if (mkey == 4) {
			ui_reclist.movePage(-1);
			checkCursor();
		}
		else if (mkey == 0) {
			ui_reclist.moveCursor(-1);
			registerRequired();
		}
		else if (mkey == 1) {
			ui_reclist.moveCursor(1);
			registerRequired();
		}
	}

	void HandleChemicalList(int mkey) {
		if (mkey == 5) {
			ui_chemlist.movePage(1);
		}
		else if (mkey == 4) {
			ui_chemlist.movePage(-1);
		}

	}	
}