class RandomChemistryDrop : RandomSpawner {
	default {
		dropItem "SulfurPickup", 200, 2;
		dropItem "HydrogenPickup", 200, 2;
		dropItem "OxygenPickup", 200, 2;
		dropItem "ZincSheetPickup", 200, 3;
		dropItem "CopperSheetPickup", 200, 3;
		dropItem "WaterPickup", 200, 5;

	}
	states {
		spawn:
			BCMA A -1;
			Stop;
	}
}

class RandomWoodDrop : RandomSpawner {
	default {
		dropItem "WoodPickup", 150, 5;
		dropItem "CharcoalPickup", 200, 10;
	}
	states {
		spawn:
			WOOD A -1;
			Stop;
	}
}

class RandomChemistryShopDrop : RandomSpawner {
	default {
		dropItem "IronOxide", 75, 1;
		dropItem "HydrochloricAcidPickup", 100, 1;
		dropItem "AmmoniumSulfatePickup", 100, 1;
		dropitem "SulphuricAcidPickup", 150, 2;
		dropItem "BatteryPickup", 200, 2;
		dropItem "ChromeAlumPickup", 200, 2;
		dropItem "HydrogenPickup", 200, 2;
		dropItem "OxygenPickup", 200, 2;
		dropItem "SodiumPickup", 200, 2;
		dropItem "SulfurPickup", 200, 2;
		dropItem "IronSulfatePickup", 200, 2;
		dropItem "HydrogenPickup", 200, 2;
		dropItem "ZincSheetPickup", 200, 3;
		dropItem "CopperSheetPickup", 200, 3;
		dropItem "WaterPickup", 200, 5;

	}
	states {
		spawn:
			ALIZ A -1;
			Stop;
	}
}