class Readable : RItem {
	property menuname: menuname;
	string menuname;
	default {
		Inventory.MaxAmount 1;
	}
	override bool Use(bool pickup) {
		GenericMenu.SetMenu(menuname);
		return false;
	}
}