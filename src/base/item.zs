
class RItem : Inventory {
	property classname: classname;
	string classname;
	default {
		+Inventory.INVBAR
		Inventory.MaxAmount 99;
		Inventory.Amount 1;
	}
	override Inventory CreateTossable(int amt) {
		let actor = Actor.spawn(self.classname, owner.pos, true);
		if (actor is "RPickup") {
			RPickup p = RPickup(actor);
			if (amt != -1) {
				p.amount = amt;
			}
			else {
				p.amount = 1;
			}
		}
		owner.TakeInventory(self.getClassName(), amt);
		return Inventory(actor);
	}
}