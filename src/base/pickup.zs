
class RPickup : Actor {
	property classname: classname;
	property amount: amount;
	property sound: sound;
	string classname;
	int amount;
	string sound;
	override bool Used(Actor who) {
		if (self.CanPickup()) {
			who.player.bonuscount = 6;
			who.GiveInventory(self.classname, self.amount);
			who.A_StartSound(self.sound);
			console.printf(self.PickupMessage());
			self.destroy();
			return true;
		}
		console.printf(self.CantPickupMessage());
		return false;
	}
	virtual bool CanPickup() { return true; }
	virtual string PickupMessage() { 
		if (self.amount > 1) {
			return string.format("Found \"%s\" (%i)", self.getTag(), self.amount); 
		}
		return string.format("\cLFound \"%s\cL\"", self.getTag()); 
	}
	virtual string CantPickupMessage() { return ""; }
}


