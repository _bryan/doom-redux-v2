class CrystalItem : RItem {
	TeleportHandler hndl;
	override void PostBeginPlay() {
		hndl = TeleportHandler(EventHandler.Find("TeleportHandler"));
	}

	Teleporter findNearest() {
		ThinkerIterator teleFinder = ThinkerIterator.Create("Teleporter");
		Teleporter mo;
		Teleporter found;
		while (mo = Teleporter(teleFinder.next())) {
			double blockdist = owner.radius + mo.radius;
			if (abs(owner.pos.x - mo.pos.x) > blockdist || abs(owner.pos.y - mo.pos.y) > blockdist) {
				continue;    
			}
			if (mo.checkSight(owner)) {
				found = mo;
				break;
			}
		}
		return found;
	}

}