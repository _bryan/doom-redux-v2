class ChemistryRecipe {
	string name;
	int timecost;
	int amount;
	String result;
	Array<String> colors;
	Array<String> descr;
	Array<ChemistryRequirement> requires;
	String randomColor() {
		return colors[random(0, colors.size() - 1)];
	}
	void addRequirement(string cls, int amt) {
		ChemistryRequirement newReq = new ("ChemistryRequirement");
		newReq.classname = cls;
		newReq.amount = amt;
		requires.push(newReq);
	}
	bool canCraft(Actor a) {
		for(int i = 0; i < requires.size(); i++) {
			ChemistryRequirement req = requires[i];
			int amount = a.countinv(req.classname);
			if (amount < req.amount) {
				return false;
			}
		}
		return true;
	}
}
