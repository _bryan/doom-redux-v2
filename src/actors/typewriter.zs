
class SaveTypewriter : Actor {

	TeleportHandler hndl;

	default {
		Radius 15;
		Height 12;
	}

	override void PostBeginPlay() {
		hndl = TeleportHandler(EventHandler.Find("TeleportHandler"));
	}

	override bool Used(Actor who) {
		hndl.lastUsed = self;
		GenericMenu.SetMenu("SaveGameMenu");
		return true;
	}

	states {
		spawn:
			TPWR A -1;
			Stop;
	}

}