class Filebox : Actor {
	default {
		Radius 8;
		Height 12;
	}
	int index;
	StateLabel labels[3];
	override void PostBeginPlay() {
		super.PostBeginPlay();
		labels[0] = "V0";
		labels[1] = "V1";
		labels[2] = "V2";
		index = random(0, 2);
	}	
	states {
		spawn:
			OBOX A 0 {
				return ResolveState(labels[index]);
			}
			Loop;
		V0: OBOX A -1; Stop;
		V1:	OBOX B -1; Stop;
		V2:	OBOX C -1; Stop;	
	}
}