class PencilHolder : Actor {
	default {
		Radius 5;
		Height 14;
		Scale 0.6;
	}
	states {
		spawn:
			PPCP A -1;
			Stop;	
	}
}