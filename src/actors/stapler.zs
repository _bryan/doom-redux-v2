class Stapler : Actor {
	default {
		Radius 8;
		Height 8;
		Scale 0.6;
	}
	int index;
	StateLabel labels[2];
	override void PostBeginPlay() {
		super.PostBeginPlay();
		labels[0] = "V0";
		labels[1] = "V1";
		index = random(0, 1);
	}	
	states {
		spawn:
			STPL A 0 {
				return ResolveState(labels[index]);
			}
			Loop;
		V0: STPL A -1; Stop;
		V1:	STPL B -1; Stop;	
	}
}