
class Teleporter : Actor {

	TeleportHandler hndl;
	bool crystalSet;
	string color;

	default {
		Radius 32;
		Height 69;
	}

	override void PostBeginPlay() {
		super.PostBeginPlay();
		hndl = TeleportHandler(EventHandler.Find("TeleportHandler"));
	}

	states {
		spawn:
			TLPT A -1;
			Stop;
		green:
			TLPT B -1;
			Stop;
		blue:
			TLPT C -1;
			Stop;
		red:
			TLPT D -1;
			Stop;
		purple:
			TLPT E -1;
			Stop;
	}

	override bool Used(Actor who) {
		if (!crystalSet) {
			return false;
		}

		if (self.color == "green") {
			Teleporter other = self == hndl.gA ? hndl.gB : hndl.gA;
			if (!other) {
				console.printf("Green isn't synchronized.");
				return false;
			}
			else {
				who.SetOrigin(other.pos, false);
				who.vel += (
					random(-8, 8),
					random(-8, 8),
					0
				);
				return true;
			}
		}
		else if (self.color == "blue") {
			Teleporter other = self == hndl.bA ? hndl.bB : hndl.bA;
			if (!other) {
				console.printf("Blue isn't synchronized.");
				return false;
			}
			else {
				who.SetOrigin(other.pos, false);
				who.vel += (
					random(-8, 8),
					random(-8, 8),
					0
				);
				hndl.bA.reset();
				hndl.bB.reset();
				hndl.bA = NULL;
				hndl.bB = NULL;
				return true;
			}
		}
		else if (self.color == "purple") {
			Teleporter other = self == hndl.pA ? hndl.pB : hndl.pA;
			if (!other) {
				console.printf("Purple isn't synchronized.");
				return false;
			}
			else {
				who.SetOrigin(other.pos, false);
				who.vel += (
					random(-8, 8),
					random(-8, 8),
					0
				);
				hndl.pA.reset();
				hndl.pB.reset();
				hndl.pA = NULL;
				hndl.pB = NULL;
				return true;
			}
		}

		return true;
	}

	bool tryGreenTeleport(Actor who) {
		return false;
	}

	void reset() {
		self.SetStateLabel("spawn");
		crystalSet = false;
		self.color = "";
	}

	bool setCrystal(CrystalItem crystal) {
		if (crystalSet) {
			return false;
		}

		if (crystal is "GreenCrystalItem") {
			self.SetStateLabel("green");
			crystalSet = true;
			self.color = "green";
			return true;
		}
		else if (crystal is "BlueCrystalItem") {
			self.SetStateLabel("blue");
			crystalSet = true;
			self.color = "blue";
			return true;
		}
		else if (crystal is "PurpleCrystalItem") {
			self.SetStateLabel("purple");
			crystalSet = true;
			self.color = "purple";
			return true;
		}

		return false;
	}


}