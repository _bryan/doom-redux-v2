
class WaterCooler : Actor {
	default {
		+SOLID
		Radius 8;
		Height 57;
	}
	states {
		spawn:
			WACL A -1;
			Stop;
	}
}