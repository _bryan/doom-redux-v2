class Taperoll : Actor {
	default {
		Radius 8;
		Height 8;
		Scale 0.6;
	}
	states {
		spawn:
			TAPE A -1;
			Stop;	
	}
}