class Bottle : Actor {
	default {
		+SHOOTABLE
		+NOBLOOD
		+PUSHABLE
		+NOTAUTOAIMED
		Mass 1000;
		Pushfactor 0.07;
		Radius 3;
		Height 8;
		Scale 0.7;
	}

	int index;
	bool broke;
	StateLabel labels[8];
	StateLabel broken[8];

	override void PostBeginPlay() {
		super.PostBeginPlay();

		labels[0] = "V0";
		labels[1] = "V1";
		labels[2] = "V2";
		labels[3] = "V3";
		labels[4] = "V4";
		labels[5] = "V5";
		labels[6] = "V6";
		labels[7] = "V7";

		broken[0] = "B0";
		broken[1] = "B1";
		broken[2] = "B2";
		broken[3] = "B3";
		broken[4] = "B4";
		broken[5] = "B5";
		broken[6] = "B6";
		broken[7] = "B7";

		index = random(0, 7);
	}	

	override int damagemobj(actor inflictor, actor source, int damage, name mod, int flags, double angle) {
		if (!broke) {
			self.A_StartSound("sfx/glass/break", CHAN_AUTO);
			self.SetStateLabel(broken[index]);
			self.A_UnSetShootable();
			broke = true;
		}
		return super.damagemobj(inflictor, source, damage, mod, flags, angle);
	}

	states {
		spawn:
			BOTE A 0 {
				return ResolveState(labels[index]);
			}
			Loop;
		V0: BOTE A -1; Stop;
		V1:	BOTE B -1; Stop;
		V2:	BOTE C -1; Stop;
		V3: BOTE D -1; Stop;
		V4:	BOTE E -1; Stop;
		V5:	BOTE F -1; Stop;	
		V6:	BOTE G -1; Stop;
		V7:	BOTE H -1; Stop;	
		B0: BXTL A -1; Stop;
		B1:	BXTL B -1; Stop;
		B2:	BXTL C -1; Stop;
		B3: BXTL D -1; Stop;
		B4:	BXTL E -1; Stop;
		B5:	BXTL F -1; Stop;	
		B6:	BXTL G -1; Stop;
		B7:	BXTL H -1; Stop;	
	}
}