class Binder : Actor {
	default {
		Radius 3;
		Height 8;
		Scale 0.7;
	}

	int index;
	StateLabel labels[6];

	override void PostBeginPlay() {
		super.PostBeginPlay();
		labels[0] = "V0";
		labels[1] = "V1";
		labels[2] = "V2";
		labels[3] = "V3";
		labels[4] = "V4";
		labels[5] = "V5";
		index = random(0, 5);
	}	

	states {
		spawn:
			BIND A 0 {
				return ResolveState(labels[index]);
			}
			Loop;
		V0: BIND A -1; Stop;
		V1:	BIND B -1; Stop;
		V2:	BIND C -1; Stop;
		V3: BIND D -1; Stop;
		V4:	BIND E -1; Stop;
		V5:	BIND F -1; Stop;	
	}
}