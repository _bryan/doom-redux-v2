class Books : Actor {
	default {
		Radius 3;
		Height 8;
		Scale 0.7;
	}

	int index;
	StateLabel labels[6];

	override void PostBeginPlay() {
		super.PostBeginPlay();
		labels[0] = "V0";
		labels[1] = "V1";
		labels[2] = "V2";
		labels[3] = "V3";
		labels[4] = "V4";
		labels[5] = "V5";
		index = random(0, 5);
	}	

	states {
		spawn:
			BOOK A 0 {
				return ResolveState(labels[index]);
			}
			Loop;
		V0: BOOK A -1; Stop;
		V1:	BOOK B -1; Stop;
		V2:	BOOK C -1; Stop;
		V3: BOOK D -1; Stop;
		V4:	BOOK E -1; Stop;
		V5:	BOOK F -1; Stop;	
	}
}