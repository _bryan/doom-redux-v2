
class Cardwriter : Actor {

	KeycardHandler hndl;

	default {
		Radius 32;
		Height 64;
		-SOLID
		-NOGRAVITY
	}

	override void PostBeginPlay() {
		super.PostBeginPlay();
		hndl = KeycardHandler(EventHandler.Find("KeycardHandler"));
	}

	override bool Used(Actor who) {
		hndl.scriptOpened[consoleplayer] = true;
		GenericMenu.SetMenu("CardWriterMenu");
		return true;
	}

	states {
		spawn:
			TNT1 A -1;
			Stop;
	}

}