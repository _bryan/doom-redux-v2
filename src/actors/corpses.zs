
class BouncingCorpse : Actor {
	default {
		radius 1;
		height 1;
	}
	action void a_springen() {
		self.vel += (
			random(-1, 1),
			random(-1, 1),
			random(1, 2)
		);
	}
	action void a_randomtic(int min, int max) {
		A_SetTics(random(min, max));
	}
}

class DeadFemaleScientist : BouncingCorpse {
	states {
		spawn: 
			FSZK N 35;
			FSZK N 1 a_randomtic(15, 35);
			FSZK N 10 a_springen();
			Loop;
	}
}

class DeadMaleScientist : BouncingCorpse {
	states {
		spawn: 
			SCZA N 35;
			SCZA N 1 a_randomtic(15, 35);
			SCZA N 10 a_springen();
			Loop;
	}
}
