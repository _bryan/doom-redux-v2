# Doom Redux (v2)

This map is a work in progress. Not all parts of the map are currently explorable or finished. It is possible though to 'complete' the map.

There is a lot going on in this map. Some attempts have been made with regards to optimisation. Be aware though that mod with lots of heavy effects or that spawn a lot
of actors might not perform optimally.

The map can be played in two ways. For players that want to blast their way through towards the end this is possible. It is not required to complete the map to play any other way than a traditional Doom map. For players that want to take it a bit slower some features have been implemented to try to facilitate something of an alternative world and story. The features aren't going to jump out in your face. As such, these state these features are in now are possibly not representative of the final product or story. Key items that are required to progress are picked up automatically by walking over them. Optional items can be picked up.

In the options menu there is an option for AI visibility through windows. Vanilla Doom human monster line attacks will not trigger windows to be broken, allowing them to seemingly shoot through them. If you are using a mod pack with enemies that fire projectiles instead of using line traces you can enable or disable this option. When disabled, enemies will only attack through windows after they have been broken. 

## For Hideous Destructor players

Be aware that using the doorbuster on doors that are locked can result in sometimes empty areas, or areas being triggered from the incorrect direction (entering backwards, for example). 

As well I would recommend disabling respawning. During the course of the map enemies are spawned when they are needed. An imp that goes around reviving all the dead units might not perform very well. 

The visiblity option also doesn't seem to have an effect, be aware enemies will shoot you through windows.

# Contributors

- Clay
- Ultra64

## Testers

- Sacha
- TG5

## Shared resources used

- Max Payne texture pack
- Counter-strike texture pack
- King pin texture pack
- Scientist sprites
- Dark (Obtained from youtube studio, lost URL)
- Secret (Obtained from youtube studio, lost URL)
- Impertinence - Joel Cummins
- Quake Small Font